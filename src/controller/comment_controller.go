// Package controller provides a base structure and functions for manage
// all controllers.
// Author:  wmb1207.
package controller

import (
	"blog/entity"
	"blog/errors"
	"blog/service"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type Comment struct {
	Controller
}

// New return the pointer to a new instance of controller.Blog.
func NewComment(s service.Servicer) *Comment {
	return &Comment{
		Controller{
			service: s,
		},
	}
}

// Get send one comment in json format with status code 200 based on the ID
// passed as parameter in the url or if an error occurs return a json with the
// error and status code 400.
// GET {url}/api/comments/:id
// Response (Success):
//	{
//		"status": 200,
//		"body": Comment
//  }
// Response (Errors):
//  {
//		"status": "error",
//		"body": error
//	}
func (cm *Comment) Get(c *gin.Context) {
	const op errors.Op = "Comment.Get"
	var result entity.ModelInterface

	paramId := c.Param("id")
	id, err := strconv.Atoi(paramId)
	if err != nil {
		err = errors.E(op, errors.KindUnprocesable, fmt.Errorf("Wrong parameter not a number..."))
		cm.HandleErr(err, c)
		return
	}

	result, err = cm.service.Find(uint(id))
	if err != nil {
		err = errors.E(op, err)
		cm.HandleErr(err, c)
		return
	}

	cm.SuccessResponse(c, http.StatusOK, &result)
}

// GetAlls send all comments in json format with status code 200
// or if an error occurs return a json with the error and status code 400.
// GET {url}/api/comments/
// Response (Success):
//	{
//		"status": 200,
//		"body": Comments
//  }
// Response (Errors):
//  {
//		"status": "error",
//		"body": error
//	}
func (cm *Comment) GetAll(c *gin.Context) {
	const op errors.Op = "Comment.GetAll"
	comments, err := cm.service.FindAll()
	if err != nil {
		err = errors.E(op, err)
		cm.HandleErr(err, c)
		return
	}

	cm.SuccessResponse(c, http.StatusOK, &comments)
}

// Create creates a new comment based on the json data received and return
// an status code 200 if an error occurs return an status code 400.
// POST {url}/api/comments/
// Example json data:
//	{
//		"body": "TEST BODY",
//		"user_id": 6,
//		"blog_id": 15
//	}
// Response (Success):
//	{
//		"status": "success",
//		"body": Comment
//	}
// Response (Errors):
//	{
//		"status": "error",
//		"error": error
//  }
func (cm *Comment) Create(c *gin.Context) {
	const op errors.Op = "Comment.Create"
	var result entity.ModelInterface
	comment := entity.Comment{}

	err := c.Bind(&comment)
	if err != nil {
		err = errors.E(op, errors.KindUnprocesable, err)
		cm.HandleErr(err, c)
		return
	}

	result, err = cm.service.Insert(comment)
	if err != nil {
		err = errors.E(op, err)
		cm.HandleErr(err, c)
		return
	}

	comment = result.(entity.Comment)
	cm.SuccessResponse(c, http.StatusOK, &comment)
}

// Delete deletes a comment based on the id passed in the URL.
// DELETE {url}/api/comments/:id.
// Response (Success):
//	{
//		"status": "success",
//	}
// Response (Errors):
//	{
//		"status": "error",
//		"error": error
//  }
func (cm *Comment) Delete(c *gin.Context) {
	const op errors.Op = "Comment.Delete"
	paramId := c.Param("id")

	id, err := strconv.Atoi(paramId)
	if err != nil {
		err = errors.E(op, errors.KindUnprocesable, err)
		cm.HandleErr(err, c)
		return
	}

	err = cm.service.Delete(uint(id))

	if err != nil {
		err = errors.E(op, err)
		cm.HandleErr(err, c)
		return
	}

	cm.SuccessResponse(c, http.StatusOK, nil)
}
