// Package controller provides a base structure and functions for manage
// all controllers.
// Author:  wmb1207.
package controller

import (
	"blog/email"
	"blog/entity"
	"blog/errors"
	"blog/service"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

// errInvalidToken Custom error.
var errInvalidToken = fmt.Errorf("Invalid token")

// Struct restorePasswordController composed by:
// - controller.Controller (Base controller struct)
//   - restorePasswordRepository RPRepository (Restore password repository)
// - userRepository.URepository (User repository)
type RestorePass struct {
	Controller
}

// New return the pointer to a new instance of restorePasswordController.
func NewRestorePass(s service.Servicer) *RestorePass {
	return &RestorePass{
		Controller{
			service: s,
		},
	}
}

// RequestPasswordReset creates a new reset link based on the json data received sends it to the
// user email and return an status code 200 if an error occurs return an status code 400.
// POST {url}/api/password/restore/
// Example json data:
//	{
//		"email": "email@service.com"
//	}
// Response (Success):
//	{
//		"status": "success"
//	}
// Response (Errors):
//	{
//		"status": "error",
//		"error": error
//  }
func (rpc *RestorePass) RequestPasswordReset(c *gin.Context) {
	const op errors.Op = "restorePasswordController.RequestPasswordReset"
	var token string

	requestRestore := entity.RestorePasswordRequest{}
	err := c.Bind(&requestRestore)
	if err != nil {
		err = errors.E(op, errors.KindUnprocesable, fmt.Errorf("Error wrong body request"))
		rpc.HandleErr(err, c)
		return
	}

	service := rpc.service.(*service.RestorePass)
	token, err = service.GenerateTemporalToken(requestRestore)
	if err = rpc.sendEmail(token, requestRestore.Email); err != nil {
		err = errors.E(op, err)
		rpc.ErrorResponse(c, http.StatusBadRequest, err)
		return
	}

	rpc.SuccessResponse(c, http.StatusOK, "")
}

// sendEmail send the email with the link to restore the password.
func (rpc *RestorePass) sendEmail(token string, mail string) error {
	const op errors.Op = "restorePasswordController.sendEmail"
	e := email.NewRestorePassword(token, mail)
	if err := e.GenerateBody(); err != nil {
		err = errors.E(op, err)
		return err
	}
	e.Send()
	return nil
}

// ResetPassword Updates the password for an specific user.
// PUT {url}/restore/password/:token
// {
// 		"email": "email@service.com",
//		"password": "password"
// }
// Response (Success):
//	{
//		"status": "success",
//		"body": User
//	}
// Response (Errors):
//	{
//		"status": "error",
//		"error": error
//  }
func (rpc *RestorePass) ResetPassword(c *gin.Context) {
	const op errors.Op = "restorePasswordController.ResetPassword"
	var user entity.ModelInterface
	request := entity.NewPasswordRequest{}

	err := c.Bind(&request)
	if err != nil {
		err = errors.E(op, errors.KindUnprocesable, fmt.Errorf("Error wrong body request"))
		rpc.HandleErr(err, c)
		return
	}

	service := rpc.service.(*service.RestorePass)
	tokenParam := c.Param("token")
	user, err = service.ResetPassword(tokenParam, request)
	if err != nil {
		rpc.ErrorResponse(c, http.StatusBadRequest, err)
		return
	}

	rpc.SuccessResponse(c, http.StatusOK, user)

}
