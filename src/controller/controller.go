// Package controller provides a base structure and functions for manage
// all controllers.
// Author:  wmb1207.
package controller

import (
	"blog/errors"
	"blog/logger"
	"blog/service"
	"log"

	"github.com/gin-gonic/gin"
)

// Interface ControllerInterface.
type ControllerInterface interface {
	ErrorResponse(c *gin.Context, code int, err interface{})
	SuccessResponse(c *gin.Context, code int, err interface{})
	Get(c *gin.Context)
	GetAll(c *gin.Context)
	Update(c *gin.Context)
	Delete(c *gin.Context)
	Create(c *gin.Context)
}

// Struct Controller composed by:
// 	- Repositoy (repository Interface).
type Controller struct {
	service service.Servicer
}

// ErrorResponse generate json response for error cases.
func (cn *Controller) ErrorResponse(c *gin.Context, code int, message interface{}) {
	c.JSON(code, gin.H{
		"status":  "failed",
		"message": message,
	})
}

// SuccessResponse generate json response for error cases.
func (cn *Controller) SuccessResponse(c *gin.Context, code int, body interface{}) {
	var response gin.H
	if body == nil {
		response = gin.H{
			"status": "success",
		}
	} else {
		response = gin.H{
			"status": "success",
			"body":   body,
		}
	}
	c.Set("responseBody", body)
	c.JSON(code, response)
}

func (cn *Controller) Log(args ...interface{}) {
	logger := logger.Logger()
	log.Println(logger)
	logger.Log(args)
}

func (cn *Controller) HandleErr(err error, c *gin.Context) {
	resErr, ok := err.(*errors.Error)
	if !ok {
		panic(err)
	}

	ops := errors.Ops(resErr)
	kind := errors.GetKind(resErr)
	message := resErr.Error()

	code := int(kind)
	cn.Log("Error", ops, code, message)
	cn.ErrorResponse(c, code, message)
}
