// Package controller provides a base structure and functions for manage
// all controllers.
// Author:  wmb1207.
package controller

import (
	"blog/entity"
	"blog/errors"
	"blog/service"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

type loginController struct {
	Controller
	signingKey       []byte
	errWrongPassword error
}

// New return the pointer to a new instance of loginController.
func NewLogin(s service.Servicer) *loginController {
	return &loginController{
		Controller{
			service: s,
		},
		[]byte(""),
		fmt.Errorf("Wrong password"),
	}
}

// Login checks the credentials and returns a jwt Token.
// POST {url}/api/login/
// Example json data:
// {
//		"email": "email@service.com",
//		"password": "secretPass"
// }
// Response (Success):
// {
//		"status": 200,
//  	"token": jwtToken,
//		"user": User
// }
// Response (Errors):
//  {
//		"status": "error",
//		"body": error
//	}
func (l *loginController) Login(c *gin.Context) {
	const op errors.Op = "loginController.Login"
	var (
		err   error
		token string
	)

	credentials := entity.LoginRequest{}
	err = c.Bind(&credentials)
	if err != nil {
		err = errors.E(op, errors.KindUnprocesable, l.errWrongPassword)
		l.HandleErr(err, c)
		return
	}

	service := l.service.(*service.User)

	response, err := l.service.FindBy("email", credentials.Email)
	if err != nil {
		err = errors.E(op, err)
		l.HandleErr(err, c)
		return
	}

	user := response.(entity.User)

	token, err = service.Login(credentials)
	if err != nil {
		err = errors.E(op, err)
		l.HandleErr(err, c)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"status": "success",
		"token":  token,
		"user":   &user,
	})
}
