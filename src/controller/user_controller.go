// Package controller provides a base structure and functions for manage
// all controllers.
// Author:  wmb1207.
package controller

import (
	"blog/email"
	"blog/entity"
	"blog/errors"
	"blog/service"
	"blog/validator"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type User struct {
	Controller
}

// New return the pointer to a new instance of controller.User.
func NewUser(s service.Servicer) *User {
	return &User{
		Controller{
			service: s,
		},
	}
}

// GetAll send all blogs in json format with status code 200
// or if an error occurs return a json with the error and status code 400.
// GET {url}/api/users/
// Response (Success):
//	{
//		"status": 200,
//		"body": Users
//  }
// Response (Errors):
//  {
//		"status": "error",
//		"body": error
//	}
func (u *User) GetAll(c *gin.Context) {
	const op errors.Op = "User.GetAll"
	users, err := u.service.FindAll()
	if err != nil {
		err = errors.E(op, err)
		u.HandleErr(err, c)
		return
	}

	u.SuccessResponse(c, http.StatusOK, &users)
}

// Get send one unser in json format with status code 200 based on the ID
// passed as parameter in the url or if an error occurs return a json with the
// error and status code 400.
// GET {url}/api/users/:id
// Response (Success):
//	{
//		"status": 200,
//		"body": User
//  }
// Response (Errors):
//  {
//		"status": "error",
//		"body": error
//	}
func (u *User) Get(c *gin.Context) {
	const op errors.Op = "User.Get"
	var result entity.ModelInterface

	paramId := c.Param("id")
	id, err := strconv.Atoi(paramId)
	if err != nil {
		err = errors.E(op, errors.KindUnprocesable, fmt.Errorf("Wrong parameter not a number..."))
		u.HandleErr(err, c)
		return
	}

	result, err = u.service.Find(uint(id))
	if err != nil {
		err = errors.E(op, err)
		u.HandleErr(err, c)
		return
	}

	u.SuccessResponse(c, http.StatusOK, &result)
}

// Create creates a new user based on the json data received and return
// an status code 200 if an error occurs return an status code 400.
// POST {url}/api/users/
// Example json data:
//	{
//		"email": "email@service.com",
// 		"user_name": "Example username",
//		"name": "Name",
//		"password": "password",
//	}
// Response (Success):
//	{
//		"status": "success",
//		"body": User
//	}
// Response (Errors):
//	{
//		"status": "error",
//		"error": error
//  }
func (u *User) Create(c *gin.Context) {
	const op errors.Op = "User.Create"
	var result entity.ModelInterface
	userRequest := entity.UserRequest{}

	err := c.Bind(&userRequest)
	if err != nil {
		err = errors.E(op, errors.KindUnprocesable, fmt.Errorf("Wrong request body"))
		u.HandleErr(err, c)
		return
	}

	// Validate the input.
	err = validator.Validate(userRequest)
	if err != nil {
		err = errors.E(op, errors.KindUnprocesable, err)
		u.HandleErr(err, c)
		return
	}

	user := *userRequest.Convert()
	result, err = u.service.Insert(user)
	if err != nil {
		err = errors.E(op, err)
		u.HandleErr(err, c)
		return
	}

	user = result.(entity.User)
	email := email.NewCreateAccountConfirmation(user.Name, user.Email)
	if err = email.GenerateBody(); err != nil {
		err = errors.E(op, err)
		u.HandleErr(err, c)
	}
	email.Send()
	u.SuccessResponse(c, http.StatusOK, user)
}

// Delete deletes a user based on the id passed in the URL.
// DELETE {url}/api/users/:id.
// Response (Success):
//	{
//		"status": "success",
//	}
// Response (Errors):
//	{
//		"status": "error",
//		"error": error
//  }
func (u *User) Delete(c *gin.Context) {
	const op errors.Op = "User.Delete"
	paramId := c.Param("id")

	id, err := strconv.Atoi(paramId)
	if err != nil {
		err = errors.E(op, errors.KindUnprocesable, fmt.Errorf("Wrong parameter not a number..."))
		u.HandleErr(err, c)
		return
	}

	err = u.service.Delete(uint(id))
	if err != nil {
		err = errors.E(op, err)
		u.HandleErr(err, c)
		return
	}

	u.SuccessResponse(c, http.StatusOK, nil)
}

// Update updates a user based on the data send in json format and the
// id past in the url.
// PUT {url}/api/user/:id
// Example json data:
//	{
//		"email": "new@email.com",
// 		"user_name": "new_username",
//		"name": "newName",
//		"password": "new_password",
//	}
// Response (Success):
//	{
//		"status": "success",
//		"body": User
//	}
// Response (Errors):
//	{
//		"status": "error",
//		"error": error
//  }
func (u *User) Update(c *gin.Context) {
	const op errors.Op = "User.Update"
	var result entity.ModelInterface
	userRequest := entity.UserRequest{}
	paramId := c.Param("id")
	id, err := strconv.Atoi(paramId)
	if err != nil {
		err = errors.E(op, errors.KindUnprocesable, fmt.Errorf("Wrong parameter not a number..."))
		u.HandleErr(err, c)
		return
	}

	err = c.Bind(&userRequest)
	if err != nil {
		err = errors.E(op, errors.KindUnprocesable, fmt.Errorf("Wrong request body"))
		u.HandleErr(err, c)
		return
	}

	user := *userRequest.Convert()
	result, err = u.service.Update(user, uint(id))
	if err != nil {
		err = errors.E(op, err)
		u.HandleErr(err, c)
		return
	}

	u.SuccessResponse(c, http.StatusOK, &result)
}
