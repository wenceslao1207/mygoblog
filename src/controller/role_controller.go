// Package controller provides a base structure and functions for manage
// all controllers.
// Author:  wmb1207.
package controller

import (
	"blog/entity"
	"blog/errors"
	"blog/service"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type Role struct {
	Controller
}

// New return the pointer to a new instance of controller.Role.
func NewRole(s service.Servicer) *Role {
	return &Role{
		Controller{
			service: s,
		},
	}
}

// GetAll send all toles in json format with status code 200
// or if an error occurs return a json with the error and status code 400.
// GET {url}/api/roles/
// Response (Success):
//	{
//		"status": 200,
//		"body": Roles
//  }
// Response (Errors):
//  {
//		"status": "error",
//		"body": error
//	}
func (r *Role) GetAll(c *gin.Context) {
	const op errors.Op = "Role.GetAll"
	roles, err := r.service.FindAll()
	if err != nil {
		err = errors.E(op, errors.KindNoContent, err)
		r.HandleErr(err, c)
		return
	}

	r.SuccessResponse(c, http.StatusOK, &roles)
}

// Get send one role in json format with status code 200 based on the ID
// passed as parameter in the url or if an error occurs return a json with the
// error and status code 400.
// GET {url}/api/roles/:id
// Response (Success):
//	{
//		"status": 200,
//		"body": Role
//  }
// Response (Errors):
//  {
//		"status": "error",
//		"body": error
//	}
func (r *Role) Get(c *gin.Context) {
	const op errors.Op = "Role.Get"
	var result entity.ModelInterface

	paramId := c.Param("id")
	id, err := strconv.Atoi(paramId)
	if err != nil {
		err = errors.E(op, errors.KindUnprocesable, fmt.Errorf("Wrong parameter, not a number..."))
		r.HandleErr(err, c)
		return
	}

	result, err = r.service.Find(uint(id))
	if err != nil {
		err = errors.E(op, err)
		r.HandleErr(err, c)
		return
	}

	r.SuccessResponse(c, http.StatusOK, &result)
}

// Create creates a new role based on the json data received and return
// an status code 200 if an error occurs return an status code 400.
// POST {url}/api/roles/
// Example json data:
//	{
//		"role_name": "role"
//	}
// Response (Success):
//	{
//		"status": "success",
//		"body": Role
//	}
// Response (Errors):
//	{
//		"status": "error",
//		"error": error
//  }
func (r *Role) Create(c *gin.Context) {
	const op errors.Op = "Role.Create"
	var result entity.ModelInterface
	role := entity.Role{}

	err := c.Bind(&role)
	if err != nil {
		err = errors.E(op, errors.KindUnprocesable, fmt.Errorf("Wrong request body"))
		r.HandleErr(err, c)
		return
	}

	result, err = r.service.Insert(role)
	if err != nil {
		err = errors.E(op, err)
		r.HandleErr(err, c)
		return
	}

	r.SuccessResponse(c, http.StatusOK, result)
}

// Delete deletes a role based on the id passed in the URL.
// DELETE {url}/api/roles/:id.
// Response (Success):
//	{
//		"status": "success",
//	}
// Response (Errors):
//	{
//		"status": "error",
//		"error": error
//  }
func (r *Role) Delete(c *gin.Context) {
	const op errors.Op = "Role.Delete"
	paramId := c.Param("id")
	id, err := strconv.Atoi(paramId)
	if err != nil {
		err = errors.E(op, errors.KindUnprocesable, fmt.Errorf("Wrong parameter not a number..."))
		r.HandleErr(err, c)
		return
	}

	err = r.service.Delete(uint(id))
	if err != nil {
		err = errors.E(op, err)
		r.HandleErr(err, c)
		return
	}

	r.SuccessResponse(c, http.StatusOK, nil)
}

// Update not implemented
func (r *Role) Update(c *gin.Context) {
	const op errors.Op = "Role.Update"
	err := fmt.Errorf("PUT Method not allowed.")
	err = errors.E(op, errors.KindMethodNotAllowed, err)
	r.HandleErr(err, c)
}
