// Package controller provides a base structure and functions for manage
// all controllers.
// Author:  wmb1207.
package controller

import (
	"blog/entity"
	"blog/errors"
	"blog/service"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type Blog struct {
	Controller
}

// New return the pointer to a new instance of controller.Blog.
func NewBlog(s service.Servicer) *Blog {
	return &Blog{
		Controller{
			service: s,
		},
	}
}

// Get send one blog in json format with status code 200 based on the ID
// passed as parameter in the url or if an error occurs return a json with the
// error and status code 400.
// GET {url}/api/blogs/:id
// Response (Success):
//	{
//		"status": 200,
//		"body": Blog
//  }
// Response (Errors):
//  {
//		"status": "error",
//		"body": error
//	}
func (b *Blog) Get(c *gin.Context) {
	const op errors.Op = "Blog.Get"
	var blog entity.ModelInterface

	paramId := c.Param("id")
	id, err := strconv.Atoi(paramId)
	if err != nil {
		err = errors.E(op, errors.KindUnprocesable, fmt.Errorf("Wrong parameter not a number..."))
		b.HandleErr(err, c)
		return
	}

	blog, err = b.service.Find(uint(id))
	if err != nil {
		err = errors.E(op, err)
		b.HandleErr(err, c)
		return
	}

	b.SuccessResponse(c, http.StatusOK, blog)
}

// GetAll send all blogs in json format with status code 200
// or if an error occurs return a json with the error and status code 400.
// GET {url}/api/blogs/
// Response (Success):
//	{
//		"status": 200,
//		"body": Blogs
//  }
// Response (Errors):
//  {
//		"status": "error",
//		"body": error
//	}
func (b *Blog) GetAll(c *gin.Context) {
	const op errors.Op = "Blog.GetAll"
	blogs, err := b.service.FindAll()
	if err != nil {
		err = errors.E(op, err)
		b.HandleErr(err, c)
		return
	}

	b.SuccessResponse(c, http.StatusOK, &blogs)
}

// Create creates a new blog based on the json data received and return
// an status code 200 if an error occurs return an status code 400.
// POST {url}/api/blogs/
// Example json data:
//	{
//		"title": "Example Title",
// 		"Body": "Example Body",
//		"user_id": 1,
//		"status": "published"
//	}
// Response (Success):
//	{
//		"status": "success",
//		"body": Blogs
//	}
// Response (Errors):
//	{
//		"status": "error",
//		"error": error
//  }
func (b *Blog) Create(c *gin.Context) {
	const op errors.Op = "Blog.CreateBlog"
	var result entity.ModelInterface

	blogRequest := entity.BlogRequest{}
	err := c.Bind(&blogRequest)
	if err != nil {
		err = errors.E(op, errors.KindUnprocesable, err)
		b.HandleErr(err, c)
		return
	}

	blog := *blogRequest.Convert()
	result, err = b.service.Insert(blog)
	if err != nil {
		err = errors.E(op, err)
		b.HandleErr(err, c)
		return
	}

	b.SuccessResponse(c, http.StatusOK, result)
}

// Delete deletes a blog based on the id passed in the URL.
// DELETE {url}/api/blogs/:id.
// Response (Success):
//	{
//		"status": "success",
//	}
// Response (Errors):
//	{
//		"status": "error",
//		"error": error
//  }
func (b *Blog) Delete(c *gin.Context) {
	const op errors.Op = "Blog.DeleteBlog"
	// paramId string with the id parameter
	paramId := c.Param("id")

	// id the id convertes to int
	id, err := strconv.Atoi(paramId)
	if err != nil {
		err = errors.E(op, errors.KindUnprocesable, err)
		b.HandleErr(err, c)
		return
	}

	err = b.service.Delete(uint(id))

	if err != nil {
		err = errors.E(op, err)
		b.HandleErr(err, c)
		return
	}

	b.SuccessResponse(c, http.StatusOK, nil)
}

// Update updates a blog based on the data send in json format and the
// id past in the url.
// PUT {url}/api/blogs/:id
// Example json data:
//	{
//		"title": "Example Title",
// 		"Body": "Example Body",
//		"user_id": 1,
//		"status": "published",
//		"comments": {},
//	}
// Response (Success):
//	{
//		"status": "success",
//		"body": Blog
//	}
// Response (Errors):
//	{
//		"status": "error",
//		"error": error
//  }
func (b *Blog) Update(c *gin.Context) {
	const op errors.Op = "Blog.UpdateBlog"
	var result entity.ModelInterface
	blog := entity.Blog{}
	paramId := c.Param("id")
	id, err := strconv.Atoi(paramId)

	if err != nil {
		err = errors.E(op, errors.KindUnprocesable, err)
		b.HandleErr(err, c)
		return
	}

	err = c.Bind(&blog)
	if err != nil {
		err = errors.E(op, errors.KindUnprocesable, err)
		b.HandleErr(err, c)
		return
	}

	result, err = b.service.Update(blog, uint(id))
	if err != nil {
		err = errors.E(op, err)
		b.HandleErr(err, c)
		return
	}

	b.SuccessResponse(c, http.StatusOK, result)
}
