// Package controller provides a base structure and functions for manage
// all controllers.
// Author:  wmb1207.
package controller

import (
	"blog/entity"
	"blog/errors"
	"blog/service"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// Struct File used to describe the request struct.
// Struct fileController composed by:
// - controller.Controller (Base controller struct)
// - service.FRepository (File repository)
// - ErrFileExists Error
// Files entity.Files
//type (
//	File struct {
//		Name      string `json:"name"`
//		Directory string `json:"directory"`
//	}

//	fileController struct {
//		controller.Controller
//		service    repository.FRepository
//		ErrFileExists error
//	}
//
//	Files entity.Files
//)

type (
	File struct {
		Controller
	}
)

// New return the pointer to a new instance of fileController.
func NewFile(s service.Servicer) *File {
	return &File{
		Controller{
			service: s,
		},
	}
}

// UploadFiles Handles the uploading of files using the form.File["upload[]"]
// and return a json with a code 200 or a code 400 if an error happened.
// POST {url}/api/files/
// Example Form:
// 	upload[]=@/{file}
// Response (Success):
//	{
//		"status": "success"
//	}
// Response (Errors):
//	{
//		"status": "error",
//		"error": error
//  }
func (f *File) UploadFiles(c *gin.Context) {
	const op errors.Op = "controller.File.UploadFiles"
	form, _ := c.MultipartForm()

	service := f.service.(*service.File)
	paths, err := service.UploadFiles(form)
	if err != nil {
		err := errors.E(op, err)
		f.HandleErr(err, c)
		return
	}

	f.SuccessResponse(c, http.StatusOK, &paths)

}

// UpdateFile Handles the update of files.
// PUT {url}/api/files/:id
// Example Form:
//  {
//		"blog_id" = 1
//  }
// Response (Success):
//	{
//		"status": "success"
//		"body": File
//	}
// Response (Errors):
//	{
//		"status": "error",
//		"error": error
//  }
func (f *File) UpdateFile(c *gin.Context) {
	const op errors.Op = "fileController.UpdateFile"
	var result entity.ModelInterface
	file := entity.File{}
	paramId := c.Param("id")
	id, err := strconv.Atoi(paramId)
	if err != nil {
		err = errors.E(op, errors.KindNotFound, fmt.Errorf("Wrong id value."))
		f.HandleErr(err, c)
		return
	}

	if err = c.Bind(&file); err != nil {
		err = errors.E(op, errors.KindUnprocesable, fmt.Errorf("Wrong body sent."))
		f.HandleErr(err, c)
		return
	}

	result, err = f.service.Update(file, uint(id))
	if err != nil {
		err = errors.E(op, err)
		f.HandleErr(err, c)
		return
	}

	f.SuccessResponse(c, http.StatusOK, &result)
}

// DeleteFile deletes a file based on the id passed in the URL.
// DELETE {url}/api/files/:id.
// Response (Success):
//	{
//		"status": "success",
//	}
// Response (Errors):
//	{
//		"status": "error",
//		"error": error
//  }
func (f *File) DeleteFile(c *gin.Context) {
	const op errors.Op = "fileController.DeleteFile"
	paramId := c.Param("id")
	id, err := strconv.Atoi(paramId)
	if err != nil {
		err = errors.E(op, errors.KindNotFound, fmt.Errorf("Wrong id value."))
		f.HandleErr(err, c)
		return
	}

	err = f.service.Delete(uint(id))

	if err != nil {
		err = errors.E(op, err)
		f.HandleErr(err, c)
		return
	}

	f.SuccessResponse(c, http.StatusOK, nil)
}
