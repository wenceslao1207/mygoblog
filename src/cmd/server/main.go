package main

import (
	cronService "blog/cron"
	router "blog/routes"
)

func main() {
	cronService := cronService.GetCronService()
	cronService.Start()
	router.StartGin()
}
