// Package cronService provides structures and functions for administrate a cron service.
// Author: wmb1207.
package cronService

import (
	"blog/cron/job"

	"github.com/robfig/cron/v3"
)

var (
	cronService cron.Cron
	cronJob     job.CronJobInterface
)

// To add new cron jobs just added here in the init function after creating them as a cron
// job, basically you need to implement the cronJob interface to use it.
// Example:
// 	- cronJob = cronJobs.GetClearRestorePasswordTokens()
func init() {
	// Intiatie a cron Service.
	cronService = *cron.New()
	// Add a new cron Job.
	cronJob = job.ClearRestorePasswordTokens() //job.GetClearRestorePasswordTokens()
	cronService.AddFunc(cronJob.Schedule(), cronJob.Job())
}

// GetCronService returns a pointer to the cron service.
func GetCronService() *cron.Cron {
	return &cronService
}
