// Package cronJobs provides structures and functions for manage jobs entities.
// Author: wmb1207.
package job

// cronFunc type to be able to return a function.
type job func()

// Interface CronJobInterface used to represent all cron jhobs.
// Any cron job must implements this interfaace in order to be used.
type CronJobInterface interface {
	Schedule() string
	Job() job
}
