// Package cronJobs provides structures and functions for manage jobs entities.
// Author: wmb1207.
package job

import (
	"blog/entity"
	"blog/service"
	"errors"
	"log"
)

var clearRestorePasswordTokensJob clearRestorePasswordTokens

// Struct ClearRestorePasswordTokens composed by:
// - service The restore password service.
// password tokens so it can get them al delete the ones that are expired.
// - schedule string
type clearRestorePasswordTokens struct {
	service  service.RestorePass
	schedule string
}

func init() {
	clearRestorePasswordTokensJob = clearRestorePasswordTokens{
		service:  *service.RestorePassService,
		schedule: "@daily",
	}
}

// GetClearRestorePasswordTokens return a pointer a new instance to
// ClearRestorePasswordTokens.
func ClearRestorePasswordTokens() *clearRestorePasswordTokens {
	return &clearRestorePasswordTokensJob
}

// GetSchedule (part of cornJob interface) return the schedule value.
func (c *clearRestorePasswordTokens) Schedule() string {
	return c.schedule
}

// GetFunc (part of cornJob interface) return the function to be execured in
// the cron job.
func (c *clearRestorePasswordTokens) Job() job {
	return func() {
		result, err := c.service.FindExpiredOnes()
		if err != nil {
			log.Println(err)
		}
		tokens, ok := result.(entity.RestorePassTokens)
		if !ok {
			err = errors.New("Something went wrong can't convert to RestorePassTokens")
			log.Println(err)
		}

		if err = c.service.BulkDelete(tokens); err != nil {
			log.Println(err)
		}
	}
}
