// Package entity provides structures to represents all the entities.
// Author: wmb1207.
package entity

type (
	// Struct User represents the user entity and thwe way it should be parsed from
	// a json content type.
	User struct {
		Model
		Email    string `json:"email" validate:"required,email"`
		UserName string `json:"user_name" validate:"required"`
		Name     string `json:"name" validate:"required"`
		Password string `json:"-" validate:"required"`
		Roles    []Role `json:"roles" gorm:"many2many:user_role"`
	}

	// Struct Users collection of users.
	Users struct {
		Collection
		Users []User
	}

	// Struct UserRequest used to bind the json data send to the controller.
	UserRequest struct {
		Email    string `json:"email" validator:"required,email"`
		UserName string `json:"user_name" validator:"required"`
		Name     string `json:"name" validator:"required"`
		Password string `json:"password" validator:"required,password"`
	}
	// Struct LoginRequest used to bind the request in the login controller.
	LoginRequest struct {
		Email    string `json:"email" validator:"email"`
		Password string `json:"password" validator:"password"`
	}

	// Struct RestrePasswordRequest used to bind the json data sent to the controller
	// to ask for a reset password link.
	RestorePasswordRequest struct {
		Email string `json: "email"`
	}

	// Struct NewPasswordRequest used to bind the json data sent to the controller
	// to update the user's password.
	NewPasswordRequest struct {
		Email    string `json: "email"`
		Password string `json: "password"`
	}
)

// Convert: return a  pointer user model based on the UserRequest struct.
func (ur *UserRequest) Convert() *User {
	return &User{
		Email:    ur.Email,
		UserName: ur.UserName,
		Name:     ur.Name,
		Password: ur.Password,
	}
}
