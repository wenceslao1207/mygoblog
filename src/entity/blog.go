// Package entity provides structures to represents all the entity.
// Author: wmb1207.
package entity

import (
	"time"
)

type (
	// Struct Blog represents the blog entity with all its relations and the way it
	// should be parsed from a json content type.
	//	- Relations with:
	//		User
	//		Comments
	//		Files
	Blog struct {
		Model
		Title        string     `json:"title" validate:"required"`
		Body         string     `json:"body" validate:"required"`
		User         User       `json:"author" validate:"required"`
		UserID       uint       `json:"user_id" validate:"required gt=0"`
		CreationDate *time.Time `json:"creation_date"`
		UpdateDate   *time.Time `json:"update_date"`
		Status       string     `json:"status" validate:"required"`
		Files        Files      `json:"pictures"`
		Comments     []Comment  `json:"comments"`
	}

	// Struct Blogs collection of blogs.
	Blogs struct {
		Collection
		Blogs []Blog
	}

	// Struct BlogRequest used to bind the request in the blog controller.
	BlogRequest struct {
		Title  string `json:"title"`
		Body   string `json:"body"`
		UserID uint   `json:"user_id"`
		Status string `json:"status"`
	}
)

func (br *BlogRequest) Convert() *Blog {
	return &Blog{
		Title:  br.Title,
		Body:   br.Body,
		UserID: br.UserID,
		Status: br.Status,
	}
}
