package entity

import (
	"blog/platform/database"
	"bytes"
	"testing"
	"time"
)

func TestAdd(t *testing.T) {
	blog := NewBlogRepo()

	blogEntry := Blog{
		Title: "Test Title",
		Body: `This is just a test body for a test entry in my blog
				so this way i learn even more.`,
		Author:       "Wenceslao 1207",
		CreationDate: time.Now(),
		UpdateDate:   time.Now(),
		Status:       "Published",
	}

	if blogTitle := testBlogField("Title", blogEntry.Title); blogTitle != "" {
		t.Error(blogTitle)
	}

	if blogBody := testBlogField("Body", blogEntry.Body); blogBody != "" {
		t.Error(blogBody)
	}

	if blogAuthor := testBlogField("Author", blogEntry.Author); blogAuthor != "" {
		t.Error(blogAuthor)
	}

	blog.Add(blogEntry)

	if len(blog.Blogs) != 1 {
		t.Error("Blog WANT NOT added")
	}

}

func testSaveBlogField(t *testing.T) {
	db := database.Database{
		Driver:   "mysql",
		Database: "blogs",
		User:     "admin",
		Password: "admin",
		Host:     "database:3306",
	}

	db.Connect()

	blogEntry := Blog{}

	db.DB.Create(&blogEntry)
}

func testBlogField(field string, value string) string {
	var buffer bytes.Buffer

	buffer.WriteString("")

	if value == "" {
		buffer.WriteString("Error in: ")
		buffer.WriteString(field)
		buffer.WriteString(" -> field empty")
	}

	return buffer.String()
}
