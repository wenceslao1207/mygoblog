// Package entity provides structures to represents all the entities.
// Author: wmb1207.
package entity

type (
	// Struct File represents the file entity in the database and the way it should be
	File struct {
		Model
		FileTitle string `json:"title"`
		FileURL   string `json:"url"`
		BlogID    uint   `json:"blog_id"`
	}

	// Struct file request used as part of the file service.
	FileRequest struct {
		Name      string `json:"name"`
		Directory string `json:"directory"`
	}

	// Struct Files slice of files.
	Files []File
)
