// Package entity provides structures to represents all the entities.
// Author: wmb1207.
package entity

type (
	// Struct Role represents a role in the database.
	Role struct {
		Model
		Name string `json:"role_name"`
	}

	// Struct Roles collection of roles.
	Roles struct {
		Collection
		Roles []Role
	}
)
