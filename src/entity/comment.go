// Package entity provides structures to represents all the entity.
// Author: wmb1207.
package entity

type (
	// Struct Comment represents the comment entity and the way it should be parsed
	// from a json content type, includes:
	// 		UserID
	//		BlogID
	Comment struct {
		Model
		Body   string `json:"body"`
		User   User   `json:"author"`
		UserID uint   `json:"user_id"`
		BlogID uint   `json:"blog_id"`
	}

	// Strcut Comments collection of comments.
	Comments struct {
		Collection
		Comments []Comment
	}
)
