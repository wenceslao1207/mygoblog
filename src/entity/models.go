// Package entity provides structures to represents all the entities.
// Author: wmb1207.
package entity

import (
	"github.com/jinzhu/gorm"
)

// Struct Collection used to represents all lists of an specific model.
// parsed from a json content type, includes the BlodID to reference the blog.
// Struct RestrePassToken represents the password restore temporal token in the
// database and how it should be parsed from a json content type.
//
// Struct RestorePassTokens collections of password temporal tokens.
// Interface ModelsInterface interface that represents all models.
type (
	ModelInterface interface {
		Empty() bool
	}

	Collection struct {
		Length int
	}

	Model gorm.Model

	RestorePassToken struct {
		Model
		Email string `json:"email"`
		Token string `json:"token"`
	}

	RestorePassTokens struct {
		Collection
		RestorePassTokens []RestorePassToken
	}
)

// Empty Check if a model is empty.
func (m Model) Empty() bool {
	if m.ID == 0 {
		return true
	}
	return false
}

// Empty Checks if a collection is empty.
func (c Collection) Empty() bool {
	if c.Length == 0 {
		return true
	}
	return false
}
