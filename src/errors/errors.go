// Package errors used to login and handle all errors.
// Author: wmb1207
package errors

import (
	"net/http"
)

// Op = Operation (string used to identify the func where the error happened)
// Kind = Category of errors
type (
	Op   string
	Kind int

	Error struct {
		Op   Op
		Kind Kind
		Err  error // Embedded error.
	}
)

// Error kinds Mainly http codes
const (
	KindNotFound         Kind = http.StatusNotFound
	KindUnauthorized     Kind = http.StatusUnauthorized
	KindNoContent        Kind = http.StatusNoContent
	KindServerError      Kind = http.StatusInternalServerError
	KindForbidden        Kind = http.StatusForbidden
	KindUnavailable      Kind = http.StatusUnavailableForLegalReasons
	KindUnprocesable     Kind = http.StatusUnprocessableEntity
	KindDuplicated       Kind = http.StatusConflict
	KindMethodNotAllowed Kind = http.StatusMethodNotAllowed
)

// Implementation of the error interface.
func (e *Error) Error() string {
	var message string
	resErr, ok := e.Err.(*Error)
	if !ok {
		return e.Err.Error()
	}

	message = resErr.Error()
	return message
}

func GetKind(e *Error) Kind {
	res := e.Kind
	if e.Kind != 0 {
		return res
	}

	subErr, ok := e.Err.(*Error)
	if !ok {
		// If this happens is an error in my code...
		panic("Error not from error type...")
	}

	return GetKind(subErr)
}

func Ops(e *Error) []Op {
	res := []Op{e.Op}

	subErr, ok := e.Err.(*Error)
	if !ok {
		return res
	}

	res = append(res, Ops(subErr)...)
	return res
}

// Return the error.
func E(args ...interface{}) error {
	e := &Error{}
	for _, arg := range args {
		switch arg := arg.(type) {
		case Op:
			e.Op = arg
		case error:
			e.Err = arg
		case Kind:
			e.Kind = arg
		default:
			panic("Bad call to function E")
		}
	}
	return e
}
