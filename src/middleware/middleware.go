// Package middleware provides structures and functions for custom middlewares.
// Author: wmb1207.
package middleware

import (
	"blog/cache"
	"blog/entity"
	"blog/repository"
	"errors"
	"log"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

const identityKey = "id"

var (
	errInvalidToken = errors.New("Invalid token")
	errForbidden    = errors.New("Forbidden")
	uRepository     = repository.NewUser()
)

// Struct middleware composed by:
// - identityKey     string
// - claimUserKey    string
// - errInvalidToken error
// - errForbidden    error
// - repository      repository User repository
// - cacheHelper     pointer to cache.CacheHelper
// Base struct for all custom middlewares including, cached, requireAuth,
// ServerError and AdminRequired.
type middleware struct {
	identityKey     string
	claimUserKey    string
	errInvalidToken error
	errForbidden    error
	repository      repository.User
	cacheHelper     *cache.CacheHelper
}

// New return the pointer to a new instance of middleware.
func New() *middleware {
	c := cache.GetCacheHelper()
	c.CacheInit()
	return &middleware{
		identityKey:     identityKey,
		claimUserKey:    "user",
		errInvalidToken: errInvalidToken,
		errForbidden:    errForbidden,
		repository:      *repository.NewUser(),
		cacheHelper:     c,
	}
}

// ServerError middleware used to recover from panics, and send a
// json containing the error and a 500 status code.
func (m *middleware) ServerError() gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				c.JSON(http.StatusInternalServerError, gin.H{
					"Error": err,
				})
			}
		}()
		c.Next()
	}
}

// Cached middleware used to cached reponses and retreive the cached
// ones so it doesn't need to get the data from the database.
func (m *middleware) Cached() gin.HandlerFunc {
	return func(c *gin.Context) {
		method := c.Request.Method
		key := c.Request.URL.String()
		if result, ok := m.cacheHelper.Get(key); ok {
			// If the mrthod is a GET then try to get the cached response.
			// If it is either a PUT, POST or DELETE, clears the cache for
			// that request.
			switch method {
			case "GET":
				c.JSON(http.StatusOK, gin.H{
					"status": "success",
					"cached": "true",
					"body":   &result,
				})
				c.Abort()
				return
			case "PUT":
				m.cacheHelper.Delete(key)
			case "POST":
				m.cacheHelper.Delete(key)
			case "DELETE":
				m.cacheHelper.Delete(key)
			}
		}

		c.Next()
		if responseBody, ok := c.Get("responseBody"); ok {
			item := cache.Item{
				Key:            key,
				Value:          responseBody,
				ExpirationTime: 0,
			}
			// Cache the response.
			if method == "GET" {
				m.cacheHelper.Set(item)
			}
		}
	}
}

// AuthRequired middleware used to prevent the access to a user not
// authenticated.
func (m *middleware) AuthRequired() gin.HandlerFunc {
	return func(c *gin.Context) {
		log.Println("akjshdkjahsdkjhasdkjhaskjdhakjsdhkjashdkashdkjahsk")
		tokenHeader := c.GetHeader("Token")
		_, err := jwt.Parse(tokenHeader, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, m.errInvalidToken
			}
			c.Abort()
			return nil, nil
		})
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"status":  "failed",
				"message": err.Error(),
			})
			c.Abort()
			return
		}
		return
	}
}

// AdminRequired Checks if the user has the role admin before
// letting it continue
func (m *middleware) AdminRequired() gin.HandlerFunc {
	return func(c *gin.Context) {
		tokenHeader := c.GetHeader("Token")
		_, err := jwt.Parse(tokenHeader, func(token *jwt.Token) (interface{}, error) {
			tokenClaims := token.Claims.(jwt.MapClaims)
			userEmail := tokenClaims[m.claimUserKey].(string)
			result, err := uRepository.FindBy("email", userEmail)
			if err != nil {
				return nil, m.errInvalidToken
			}
			user := result.(entity.User)
			roles := user.Roles

			if m.checkAdminRole(roles) {
				return nil, m.errInvalidToken
			}

			return nil, nil
		})
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"status":  "failed",
				"message": m.errForbidden.Error(),
			})
			c.Abort()
			return
		}

	}
}

// checkAdminRole function used to check if the role "Admin",
// its inside the slice of roles.
func (m *middleware) checkAdminRole(roles []entity.Role) bool {
	for _, role := range roles {
		if role.Name == "Admin" {
			return true
		}
	}
	return false
}
