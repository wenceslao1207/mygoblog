// Package roleRepository provides structures and functions for manage roles entities query.
// Author: wmb1207.
// Nee more work in this. doesn't use the main repository.
package repository

import (
	"blog/database"
	"blog/entity"
	"blog/errors"
	"fmt"
	"time"
)

// Struct Role composed by:
// - repository Repository (Base repository)
// Implements repository Interface
//	FindAll() (entity.ModelInterface, error)
//	Find(id uint) (entity.ModelInterface, error)
//	Insert(model entity.ModelInterface) (entity.ModelInterface, error)
//	Delete(id uint) error
//	Update(model entity.ModelInterface, id uint) (entity.ModelInterface, error)
type Role Repository

// New return a pointer to a new instance of Role.
func NewRole() *Role {
	return &Role{
		DB:                 database.MysqlDB(),
		ErrNotFound:        fmt.Errorf("Role/s not found"),
		ErrInvalidID:       fmt.Errorf("Invalid ID"),
		ErrInvalidBody:     fmt.Errorf("Invalid Request Body"),
		ErrInsertionFailed: fmt.Errorf("Error in roles insertion"),
		ErrUpdateFailed:    fmt.Errorf("Error in roles update"),
		ErrDeleteFailed:    fmt.Errorf("Error in roles delete"),
		ErrDuplicated:      fmt.Errorf("Error duplicated roles"),
	}
}

// Find Get all roles entity.
func (r *Role) FindAll() (entity.ModelInterface, error) {
	const op errors.Op = "roleRepository.FindAll"
	roles := &entity.Roles{}

	db := r.DB.(*database.Mysql).DB
	db.Find(&roles.Roles)

	if len(roles.Roles) == 0 {
		err := errors.E(op, errors.KindNoContent, r.ErrNotFound)
		return nil, err
	}

	return roles, nil
}

// Find Get a role entity based on the id.
func (r *Role) Find(id uint) (entity.ModelInterface, error) {
	const op errors.Op = "roleRepository.Find"
	role := &entity.Role{}

	db := r.DB.(*database.Mysql).DB
	db.Find(&role, id)

	if role.Empty() {
		err := errors.E(op, errors.KindNotFound, r.ErrNotFound)
		return nil, err
	}

	return role, nil
}

// Get a role based on the name.
func (r *Role) FindBy(field string, value string) (entity.ModelInterface, error) {
	const op errors.Op = "roleRepository.GetByName"
	role := entity.Role{}

	db := r.DB.(*database.Mysql).DB
	db.Where(fmt.Sprintf("%s = ?", field), value).First(&role)

	if role.Empty() {
		err := errors.E(op, errors.KindNotFound, r.ErrNotFound)
		return role, err
	}

	return role, nil
}

// Insert Creates a new role in the Database.
func (r *Role) Insert(request entity.ModelInterface) (entity.ModelInterface, error) {
	const op errors.Op = "roleRepository.Insert"
	var role entity.Role
	currentTime := time.Now()
	role = request.(entity.Role)

	_, err := r.FindBy("name", role.Name)
	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}

	role.CreatedAt = currentTime
	role.UpdatedAt = currentTime

	db := r.DB.(*database.Mysql).DB
	db.Create(&role)

	return role, nil
}

// Update updated a role based on the id value, and the struct passed.
func (r *Role) Update(requestModel entity.ModelInterface, id uint) (entity.ModelInterface, error) {
	const op errors.Op = "roleRepository.Update"
	entityResult, err := r.Find(id)
	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}

	role := entityResult.(entity.Role)
	request := requestModel.(entity.Role)

	if request.Name != "" {
		role.Name = request.Name
	}

	role.UpdatedAt = time.Now()
	db := r.DB.(*database.Mysql).DB
	db.Save(&role)
	return role, nil

}

// Delete deletes a role based on the id value.
func (r *Role) Delete(id uint) error {
	const op errors.Op = "roleRepository.Delete"
	role := entity.Role{}
	role.ID = id

	_, err := r.Find(id)
	if err != nil {
		err = errors.E(op, err)
		return err
	}

	db := r.DB.(*database.Mysql).DB
	db.Delete(&role)
	return nil
}
