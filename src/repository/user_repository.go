// Package blogRepository provides structures and functions for manage blogs entities query.
// Author: wmb1207.
package repository

import (
	"blog/database"
	"blog/entity"
	"blog/errors"
	"fmt"
	"time"

	"golang.org/x/crypto/bcrypt"
)

// Struct User composed by:
// - repository Repository (Base repository)
// Implements repository Interface
//	FindAll() (entity.ModelInterface, error)
//	Find(id uint) (entity.ModelInterface, error)
//	Insert(model entity.ModelInterface) (entity.ModelInterface, error)
//	Delete(id uint) error
//	Update(model entity.ModelInterface, id uint) (entity.ModelInterface, error)
type User Repository

// HasAndSalt returns a hashed password and an error.
func HashAndSalt(pwd []byte) (string, error) {
	hash, err := bcrypt.GenerateFromPassword(pwd, bcrypt.MinCost)
	if err != nil {
		return "", err
	}
	return string(hash), nil
}

// New return a pointer to a new instance of User.
func NewUser() *User {
	return &User{
		DB:                 database.MysqlDB(),
		ErrNotFound:        fmt.Errorf("User not found"),
		ErrInvalidID:       fmt.Errorf("Invalid ID"),
		ErrInvalidBody:     fmt.Errorf("Invalid Request Body"),
		ErrInsertionFailed: fmt.Errorf("Error in user insertion"),
		ErrUpdateFailed:    fmt.Errorf("Error in user update"),
		ErrDeleteFailed:    fmt.Errorf("Error in user delete"),
		ErrDuplicated:      fmt.Errorf("Error duplicated user"),
	}
}

// Find Get a user entity based on the id.
func (u *User) Find(id uint) (entity.ModelInterface, error) {
	const op errors.Op = "userRepository.Find"
	user := &entity.User{}
	db := u.DB.(*database.Mysql).DB
	db.Find(&user, id)

	if user.Empty() {
		err := errors.E(op, errors.KindNotFound, u.ErrNotFound)
		return nil, err
	}

	return *user, nil
}

// Find Get all users entity.
func (u *User) FindAll() (entity.ModelInterface, error) {
	const op errors.Op = "usersRepository.FindAll"
	users := &entity.Users{}

	db := u.DB.(*database.Mysql).DB
	db.Find(&users.Users)

	if len(users.Users) == 0 {
		err := errors.E(op, errors.KindNoContent, u.ErrNotFound)
		return nil, err
	}

	users.Length = len(users.Users)
	return users, nil
}

// Find Get a user entity based on the email.
func (u *User) FindBy(field string, value string) (entity.ModelInterface, error) {
	const op errors.Op = "userRepository.GetByEmail"
	user := entity.User{}
	db := u.DB.(*database.Mysql).DB
	db.Where(fmt.Sprintf("%s = ?", field), value).First(&user)

	if user.Empty() {
		err := errors.E(op, errors.KindNotFound, u.ErrNotFound)
		return user, err
	}

	return user, nil
}

// Insert Creates a new user in the Database.
func (u *User) Insert(request entity.ModelInterface) (entity.ModelInterface, error) {
	const op errors.Op = "userRepository.Insert"
	var pwd string
	var user entity.User
	currentTime := time.Now()
	user = request.(entity.User)

	_, err := u.FindBy("emial", (user.Email))

	if err == nil {
		err = errors.E(op, errors.KindDuplicated, u.ErrDuplicated)
		return nil, err
	}

	user.CreatedAt = currentTime
	user.UpdatedAt = currentTime
	pwd, err = HashAndSalt([]byte(user.Password))
	if err != nil {
		err = errors.E(op, errors.KindServerError, fmt.Errorf("Error performing password hash"))
		return nil, err
	}

	user.Password = pwd
	db := u.DB.(*database.Mysql).DB
	db.Create(&user)
	return user, nil
}

// Delete deletes a user based on the id value.
func (u *User) Delete(id uint) error {
	const op errors.Op = "userRepository.Delete"
	user := entity.User{}
	user.ID = id

	_, err := u.Find(id)
	if err != nil {
		err = errors.E(op, err)
		return err
	}
	db := u.DB.(*database.Mysql).DB
	db.Delete(&user)
	return nil
}

// Update updated a user based on the id value, and the struct passed.
func (u *User) Update(requestModel entity.ModelInterface, id uint) (entity.ModelInterface, error) {
	const op errors.Op = "userRepository.Update"
	var pwd string
	entityResult, err := u.Find(id)
	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}
	user := entityResult.(entity.User)
	request := requestModel.(entity.User)
	if request.Email != "" {
		user.Email = request.Email
	}
	if request.UserName != "" {
		user.UserName = request.UserName
	}
	if request.Name != "" {
		user.Name = request.Name
	}
	if request.Password != "" {
		pwd, err = HashAndSalt([]byte(request.Password))
		if err != nil {
			err = errors.E(op, errors.KindServerError, fmt.Errorf("Error performing password hash"))
			return nil, err
		}
		user.Password = pwd
	}
	user.UpdatedAt = time.Now()
	db := u.DB.(*database.Mysql).DB
	db.Save(&user)
	return user, nil
}
