// Package repository.Comment provides structures and functions for manage comment entities query.
// Author: wmb1207.
package repository

import (
	"blog/database"
	"blog/entity"
	"blog/errors"
	"fmt"
	"time"
)

// Struct Comment composed by:
// - repository Repository (Based repository)
// Implements repository Interface
//	FindAll() (entity.ModelInterface, error)
//	Find(id uint) (entity.ModelInterface, error)
//	Insert(model entity.ModelInterface) (entity.ModelInterface, error)
//	Delete(id uint) error
//	Update(model entity.ModelInterface, id uint) (entity.ModelInterface, error)
type Comment Repository

// New return a pointer to a new instance of Comment
func NewComment() *Comment {
	return &Comment{
		DB:                 database.MysqlDB(),
		ErrNotFound:        fmt.Errorf("No Comment/s found"),
		ErrInvalidID:       fmt.Errorf("Invalid ID"),
		ErrInvalidBody:     fmt.Errorf("Invalid Request Body"),
		ErrInsertionFailed: fmt.Errorf("Error in comment insertion"),
		ErrUpdateFailed:    fmt.Errorf("Error in comment update"),
		ErrDeleteFailed:    fmt.Errorf("Error in comment delete"),
		ErrDuplicated:      fmt.Errorf("Error duplicated comment"),
	}
}

// Find Get a comment entity based on the id.
func (c *Comment) Find(id uint) (entity.ModelInterface, error) {
	const op errors.Op = "repository.Comment.Find"
	comment := entity.Comment{}
	db := c.DB.(*database.Mysql).DB
	db.Find(&comment, id)

	if comment.Empty() {
		err := errors.E(op, errors.KindNotFound, c.ErrNotFound)
		return nil, err
	}

	return comment, nil
}

// Find Get all comments entity.
func (c *Comment) FindAll() (entity.ModelInterface, error) {
	const op errors.Op = "repository.Comment.FindAll"
	comments := entity.Comments{}
	db := c.DB.(*database.Mysql).DB
	db.Preload("User").Find(&comments.Comments)
	if len(comments.Comments) == 0 {
		err := errors.E(op, errors.KindNoContent, c.ErrNotFound)
		return nil, err
	}

	return comments, nil
}

// Insert Creates a new comment in the Database.
func (c *Comment) Insert(request entity.ModelInterface) (entity.ModelInterface, error) {
	const op errors.Op = "repository.Comment.Insert"
	currentTime := time.Now()
	comment := request.(entity.Comment)
	comment.CreatedAt = currentTime
	comment.UpdatedAt = currentTime
	db := c.DB.(*database.Mysql).DB
	db.Create(&comment)

	if comment.Empty() {
		err := errors.E(op, errors.KindServerError, c.ErrInsertionFailed)
		return nil, err
	}

	return comment, nil
}

// Delete deletes a comment based on the id value.
func (c *Comment) Delete(id uint) error {
	const op errors.Op = "repository.Comment.Delete"
	comment := entity.Comment{}
	comment.ID = id

	if _, err := c.Find(id); err != nil {
		err = errors.E(op, errors.KindNotFound, err)
		return err
	}

	db := c.DB.(*database.Mysql).DB
	db.Delete(&comment)
	return nil
}

// Update updated a comment based on the id value, and the struct passed.
func (c *Comment) Update(requestModel entity.ModelInterface, id uint) (entity.ModelInterface, error) {
	const op errors.Op = "repository.Comment.Update"

	entityResult, err := c.Find(id)
	if err != nil {
		err = errors.E(op, errors.KindNotFound, err)
		return nil, err
	}
	comment := entityResult.(entity.Comment)
	request := requestModel.(entity.Comment)
	if request.Body != "" {
		comment.Body = request.Body
	}
	comment.UpdatedAt = time.Now()
	db := c.DB.(*database.Mysql).DB
	db.Save(&comment)
	return comment, nil
}

func (c *Comment) FindBy(field string, value string) (entity.ModelInterface, error) {
	const op errors.Op = "repository.Comment.FindBy"
	err := errors.E(op, errors.KindUnavailable, fmt.Errorf("Method Undefined in comment service."))
	return nil, err
}
