// Package repository provides structures and functions for manage entities query.
// Author: wmb1207.
package repository

import (
	"blog/database"
	"blog/entity"
)

type Repository struct {
	DB                 database.Databaser
	ErrNotFound        error
	ErrInvalidID       error
	ErrInvalidBody     error
	ErrInsertionFailed error
	ErrUpdateFailed    error
	ErrDeleteFailed    error
	ErrDuplicated      error
}

// Interface RespositoryInterface
type RepositoryInterface interface {
	FindAll() (entity.ModelInterface, error)
	Find(id uint) (entity.ModelInterface, error)
	FindBy(field string, value string) (entity.ModelInterface, error)
	Insert(model entity.ModelInterface) (entity.ModelInterface, error)
	Delete(id uint) error
	Update(model entity.ModelInterface, id uint) (entity.ModelInterface, error)
}

var (
	BlogRepo        *Blog
	UserRepo        *User
	RoleRepo        *Role
	CommentRepo     *Comment
	FileRepo        *File
	RestorePassRepo *RestorePass
)

func init() {
	BlogRepo = NewBlog()
	UserRepo = NewUser()
	RoleRepo = NewRole()
	CommentRepo = NewComment()
	FileRepo = NewFile()
	RestorePassRepo = NewRestorePass()

}
