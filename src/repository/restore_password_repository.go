// Package repository.Comment provides structures and functions for manage restore password entities query.
// Author: wmb1207.
package repository

import (
	"blog/database"
	"blog/entity"
	"blog/errors"
	"fmt"
	"time"
)

// Struct RestorePass composed by:
// - repository Repository (Base repository)
// Implements repository Interface
//	FindAll() (entity.ModelInterface, error)
//	Find(id uint) (entity.ModelInterface, error)
//	Insert(model entity.ModelInterface) (entity.ModelInterface, error)
//	Delete(id uint) error
//	Update(model entity.ModelInterface, id uint) (entity.ModelInterface, error)
type RestorePass Repository

// New return a pointer to a new instance of RestorePass.
func NewRestorePass() *RestorePass {
	return &RestorePass{
		DB:                 database.MysqlDB(),
		ErrNotFound:        fmt.Errorf("Token not found"),
		ErrInvalidID:       fmt.Errorf("Invalid ID"),
		ErrInvalidBody:     fmt.Errorf("Invalid Request Body"),
		ErrInsertionFailed: fmt.Errorf("Error in token insertion"),
		ErrUpdateFailed:    fmt.Errorf("Error in token update"),
		ErrDeleteFailed:    fmt.Errorf("Error in token delete"),
		ErrDuplicated:      fmt.Errorf("Error duplicated token"),
	}
}

// Find Get a restore password entity based on the id.
func (rpr *RestorePass) Find(id uint) (entity.ModelInterface, error) {
	const op errors.Op = "restorePasswordRepository.Find"
	restorePassToken := &entity.RestorePassToken{}
	db := rpr.DB.(*database.Mysql).DB
	db.Find(&restorePassToken, id)

	if restorePassToken.Empty() {
		err := errors.E(op, errors.KindNotFound, rpr.ErrNotFound)
		return nil, err
	}

	return restorePassToken, nil
}

// Find Get all restore token entity.
func (rpr *RestorePass) FindAll() (entity.ModelInterface, error) {
	const op errors.Op = "restorePasswordRepository.FindAll"
	restorePassTokens := entity.RestorePassTokens{}
	db := rpr.DB.(*database.Mysql).DB
	db.Find(&restorePassTokens.RestorePassTokens)

	if len(restorePassTokens.RestorePassTokens) == 0 {
		err := errors.E(op, errors.KindNoContent, rpr.ErrNotFound)
		return nil, err
	}

	restorePassTokens.Length = len(restorePassTokens.RestorePassTokens)
	return restorePassTokens, nil

}

// FindBy is Not implemented.
func (rpr *RestorePass) FindBy(field string, value string) (entity.ModelInterface, error) {
	const op errors.Op = "repository.RestorePass.FindBy"
	err := errors.E(op, errors.KindUnavailable, fmt.Errorf("Method Undefined in restorePass service."))
	return nil, err
}

// GetByEmail Get a token based on the email of the user.
func (rpr *RestorePass) GetByEmail(email string) (entity.ModelInterface, error) {
	const op errors.Op = "restorePasswordRepository.GetByEmail"
	restorePassToken := &entity.RestorePassToken{}
	db := rpr.DB.(*database.Mysql).DB
	db.Where("email = ?", email).First(&restorePassToken)

	if restorePassToken.Empty() {
		err := errors.E(op, errors.KindNotFound, rpr.ErrNotFound)
		return nil, err
	}

	return restorePassToken, nil
}

// Insert Creates a new restore token in the Database.
func (rpr *RestorePass) Insert(request entity.ModelInterface) (entity.ModelInterface, error) {
	const op errors.Op = "restorePasswordRepository.Insert"
	var (
		restorePassToken entity.RestorePassToken
		ok               bool
	)

	currentTime := time.Now()
	if restorePassToken, ok = request.(entity.RestorePassToken); !ok {
		err := errors.E(op, errors.KindUnprocesable, rpr.ErrInvalidBody)
		return nil, err
	}

	restorePassToken.CreatedAt = currentTime
	restorePassToken.UpdatedAt = currentTime

	db := rpr.DB.(*database.Mysql).DB
	db.Create(&restorePassToken)
	return restorePassToken, nil
}

// Delete deletes a restore token based on the id value.
func (rpr *RestorePass) Delete(id uint) error {
	const op errors.Op = "restorePasswordToken.Delete"
	restorePassToken := entity.RestorePassToken{}
	restorePassToken.ID = id

	_, err := rpr.Find(id)
	if err != nil {
		err = errors.E(op, err)
		return err
	}

	db := rpr.DB.(*database.Mysql).DB
	db.Delete(&restorePassToken)
	return nil
}

// Update not implemented only defined to implement the repository interface
func (rpr *RestorePass) Update(request entity.ModelInterface, id uint) (entity.ModelInterface, error) {
	const op errors.Op = "restorePasswordRepository.Update"
	errString := fmt.Errorf("Method undefined")
	err := errors.E(op, errors.KindUnavailable, errString)
	return nil, err
}
