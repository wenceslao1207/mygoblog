// Package blogRepository provides structures and functions for manage blogs entities query.
// Author: wmb1207.
package repository

import (
	"blog/database"
	"blog/entity"
	"blog/errors"
	"fmt"
	"time"
)

// Struct Blog composed by:
// - repository Repository (Base repository)
// Implements repository Interface
//	FindAll() (entity.ModelInterface, error)
//	Find(id uint) (entity.ModelInterface, error)
//	Insert(model entity.ModelInterface) (entity.ModelInterface, error)
//	Delete(id uint) error
//	Update(model entity.ModelInterface, id uint) (entity.ModelInterface, error)
type Blog Repository

// New return a pointer to a new instance of Blog.
func NewBlog() *Blog {
	return &Blog{
		DB:                 database.MysqlDB(),
		ErrNotFound:        fmt.Errorf("No blog/s found"),
		ErrInvalidID:       fmt.Errorf("Invalid ID"),
		ErrInvalidBody:     fmt.Errorf("Invalid Request Body"),
		ErrInsertionFailed: fmt.Errorf("Error in blog insertion, can't persist"),
		ErrUpdateFailed:    fmt.Errorf("Error in blog update"),
		ErrDeleteFailed:    fmt.Errorf("Error in blog delete"),
		ErrDuplicated:      fmt.Errorf("Error duplicated blog"),
	}
}

// FindBy is Not implemented.
func (b *Blog) FindBy(field string, value string) (entity.ModelInterface, error) {
	const op errors.Op = "repository.Blog.FindBy"
	err := errors.E(op, errors.KindUnavailable, fmt.Errorf("Method Undefined in comment service."))
	return nil, err
}

// Find Get a blog entity based on the id.
func (b *Blog) Find(id uint) (entity.ModelInterface, error) {
	const op errors.Op = "blogRepository.Find"
	blog := entity.Blog{}
	db := b.DB.(*database.Mysql).DB
	db.Find(&blog, id)
	if blog.Empty() {
		err := errors.E(op, errors.KindNotFound, b.ErrNotFound)
		return nil, err
	}

	return blog, nil
}

// FindAll Get all blogs entity.
func (b *Blog) FindAll() (entity.ModelInterface, error) {
	const op errors.Op = "blogRepository.FindAll"
	blogs := &entity.Blogs{}
	db := b.DB.(*database.Mysql).DB
	db.Preload("User").Preload("Comments").Find(&blogs.Blogs)
	if len(blogs.Blogs) == 0 {
		err := errors.E(op, errors.KindNoContent, b.ErrNotFound)
		return nil, err
	}

	blogs.Length = len(blogs.Blogs)
	return blogs, nil
}

// Insert Creates a new blog in the Database.
func (b *Blog) Insert(request entity.ModelInterface) (entity.ModelInterface, error) {
	const op errors.Op = "blogRespository.Insert"
	currentTime := time.Now()
	blog := request.(entity.Blog)
	blog.CreatedAt = currentTime
	blog.UpdatedAt = currentTime
	db := b.DB.(*database.Mysql).DB
	db.Create(&blog)

	if blog.Empty() {
		err := errors.E(op, errors.KindServerError, b.ErrInsertionFailed)
		return nil, err
	}

	return blog, nil
}

// Delete deletes a blog based on the id value.
func (b *Blog) Delete(id uint) error {
	const op errors.Op = "blogRepository.Delete"
	blog := entity.Blog{}
	blog.ID = id

	if _, err := b.Find(id); err != nil {
		err = errors.E(op, errors.KindNotFound, err)
		return err
	}
	db := b.DB.(*database.Mysql).DB
	db.Delete(&blog)
	return nil
}

// Update updated a blog based on the id value, and the struct passed.
func (b *Blog) Update(requestModel entity.ModelInterface, id uint) (entity.ModelInterface, error) {
	const op errors.Op = "blogRepository.Update"

	entityResult, err := b.Find(id)
	if err != nil {
		err = errors.E(op, errors.KindNotFound, err)
		return nil, err
	}

	blog := entityResult.(entity.Blog)
	request := requestModel.(entity.Blog)

	if request.Title != "" {
		blog.Title = request.Title
	}

	if request.Body != "" {
		blog.Body = request.Body
	}

	blog.UpdatedAt = time.Now()
	db := b.DB.(*database.Mysql).DB
	db.Save(&blog)
	return blog, nil
}
