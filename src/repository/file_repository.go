// Package blogRepository provides structures and functions for manage blogs entities query.
// Author: wmb1207.
package repository

import (
	"blog/database"
	"blog/entity"
	"blog/errors"
	"fmt"
	"time"
)

// Struct File composed by:
// - repository Repository (Base repository)
// Implements repository Interface
//	FindAll() (entity.ModelInterface, error)
//	Find(id uint) (entity.ModelInterface, error)
//	Insert(model entity.ModelInterface) (entity.ModelInterface, error)
//	Delete(id uint) error
//	Update(model entity.ModelInterface, id uint) (entity.ModelInterface, error)
type File Repository

// New return a pointer to a new instance of File
func NewFile() *File {
	return &File{
		DB:                 database.MysqlDB(),
		ErrNotFound:        fmt.Errorf("No file/s found"),
		ErrInvalidID:       fmt.Errorf("Invalid ID"),
		ErrInvalidBody:     fmt.Errorf("Invalid Request Body"),
		ErrInsertionFailed: fmt.Errorf("Error in file insertion, can't persist"),
		ErrUpdateFailed:    fmt.Errorf("Error in file update"),
		ErrDeleteFailed:    fmt.Errorf("Error in file delete"),
		ErrDuplicated:      fmt.Errorf("Error duplicated file"),
	}
}

// FindBy is Not implemented.
func (f *File) FindBy(field string, value string) (entity.ModelInterface, error) {
	const op errors.Op = "repository.File.FindBy"
	err := errors.E(op, errors.KindUnavailable, fmt.Errorf("Method Undefined in comment service."))
	return nil, err
}

// Find All is not implemented.
func (f *File) FindAll() (entity.ModelInterface, error) {
	const op errors.Op = "repository.File.FindAll"
	err := errors.E(op, errors.KindUnavailable, fmt.Errorf("Method Undefined in comment service."))
	return nil, err
}

// Insert a new file in the database.
func (f *File) Insert(request entity.ModelInterface) (entity.ModelInterface, error) {
	const op errors.Op = "fileRepository.Insert"
	file := request.(entity.File)
	currentTime := time.Now()
	file.CreatedAt = currentTime
	file.UpdatedAt = currentTime

	db := f.DB.(*database.Mysql).DB
	db.Create(&file)
	if file.Empty() {
		errString := fmt.Errorf("Error in file insertion")
		err := errors.E(op, errors.KindUnprocesable, errString)
		return file, err
	}

	return file, nil
}

// Delete deletes a file based on the id value.
func (f *File) Delete(id uint) error {
	const op errors.Op = "fileRepository.Delete"
	file := entity.File{}
	file.ID = id

	if _, err := f.Find(id); err != nil {
		err = errors.E(op, err)
		return err
	}
	db := f.DB.(*database.Mysql).DB
	db.Delete(&file)
	return nil
}

// Find Get a File entity based on the id.
func (f *File) Find(id uint) (entity.ModelInterface, error) {
	const op errors.Op = "fileRepository.GetById"
	file := entity.File{}
	db := f.DB.(*database.Mysql).DB
	db.Find(&file, id)
	if file.Empty() {
		errString := fmt.Errorf("File not found")
		err := errors.E(op, errors.KindNotFound, errString)
		return file, err
	}

	return file, nil
}

// Update updates a File based on the id value, and the struct passed.
func (f *File) Update(request entity.ModelInterface, id uint) (entity.ModelInterface, error) {
	const op errors.Op = "fileRepository.Update"
	fileRequest := request.(entity.File)
	result, err := f.Find(id)
	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}

	file := result.(entity.File)

	if fileRequest.BlogID != 0 {
		file.BlogID = fileRequest.BlogID
	}

	file.UpdatedAt = time.Now()
	db := f.DB.(*database.Mysql).DB
	db.Save(&file)
	return file, nil
}
