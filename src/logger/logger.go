// Package logger contains strcuts and function to manage loggin.
// Author: wmb1207
package logger

import (
	"blog/config"
	"bytes"
	"fmt"
	"log"
	"os"
	"time"
)

// Struct logger used to log in the console if the env is dev
// or to a file define in the config json if the env is prod.
type logger struct {
	Env       string
	Logger    log.Logger
	Directory string
}

// logeerInstance instance of the loggin service.
var loggerInstance logger

// logginBuffer buffer where all logs will be written.
var logginBuffer bytes.Buffer

func init() {
	cfg := config.GetConfig()

	log.Println(cfg)
	loggerInstance = logger{
		Env:       cfg.Env,
		Logger:    *log.New(&logginBuffer, "BlogApp: ", 1),
		Directory: cfg.LogDir,
	}
}

// GetLogger return a pointer to the logger service.
func Logger() *logger {
	return &loggerInstance
}

// fileName return the file used to save the logs.
func (l *logger) fileName() string {
	currentDate := time.Now()
	date := currentDate.Format("01-02-2006")
	logFile := l.Directory + "/" + date
	return logFile
}

// writeLogFile, opens / creates the log file and writes the log into that file.
func (l *logger) writeLogFile(log string) error {
	fileName := l.fileName()
	fileName = fileName + ".log"
	log = log + "\n"
	f, err := os.OpenFile(fileName, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	if _, err = f.Write([]byte(log)); err != nil {
		f.Close() // ignore error; Write error takes precedence
		return err
	}
	if err := f.Close(); err != nil {
		return err
	}
	return nil
}

func (l *logger) Log(args ...interface{}) {

	for _, arg := range args {
		l.Logger.Println(arg)
	}

	switch l.Env {
	case "dev":
		fmt.Println(logginBuffer.String())
	case "prod":
		log := logginBuffer.String()
		if err := l.writeLogFile(log); err != nil {
			panic(err)
		}
	}

	logginBuffer.Reset()
}
