// Package jwtToken provides helper functions for working with jwt token.
// Author: wmb1207.
package jwtToken

import (
	"time"

	"github.com/dgrijalva/jwt-go"
)

// GenerateJWT creates a jwt token based on the password + the email.
func GenerateJWT(email string, password string) (string, error) {
	signingKey := []byte(password)
	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)
	claims["authorized"] = true
	claims["user"] = email
	claims["exp"] = time.Now().Add(time.Minute * 30).Unix()

	tokenString, err := token.SignedString(signingKey)

	if err != nil {
		return "", err
	}

	return tokenString, nil
}
