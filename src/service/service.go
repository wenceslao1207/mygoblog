// Package Serivce defines all structs and functions for the service layer.
// Author: wmb1207
package service

import (
	"blog/entity"
	"blog/repository"
)

type (
	Servicer interface {
		repository.RepositoryInterface
		Validate(entity entity.ModelInterface) error
	}

	service struct {
		repo repository.RepositoryInterface
	}
)

var (
	BlogService        *Blog
	UserService        *User
	RoleService        *Role
	CommentService     *Comment
	FileService        *File
	RestorePassService *RestorePass
)

func init() {
	BlogService = NewBlog(repository.BlogRepo, repository.UserRepo)
	UserService = NewUser(repository.UserRepo)
	RoleService = NewRole(repository.RoleRepo)
	CommentService = NewComment(repository.CommentRepo, repository.UserRepo)
	FileService = NewFile(repository.FileRepo)
	RestorePassService = NewRestorePass(repository.RestorePassRepo, repository.UserRepo)
}
