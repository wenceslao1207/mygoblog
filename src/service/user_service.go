// Package Serivce defines all structs and functions for the service layer.
// Author: wmb1207
package service

import (
	"blog/entity"
	"blog/errors"
	"blog/repository"
	jwtToken "blog/token"
	"blog/validator"
	"fmt"

	"golang.org/x/crypto/bcrypt"
)

// Struct User used to represent the user service.
// Implements service Interface
//	FindAll() (entity.ModelInterface, error)
//	Find(id uint) (entity.ModelInterface, error)
//	Insert(model entity.ModelInterface) (entity.ModelInterface, error)
//	Delete(id uint) error
//	Update(model entity.ModelInterface, id uint) (entity.ModelInterface, error)
//	FindBy(field string, value string) (entity.ModelInterface, error)
//  Validate(entity entity.ModelInterface) error
type User service

// NewUser Return a pointer to a new instance of service.User.
func NewUser(repo *repository.User) *User {
	return &User{
		repo: repo,
	}
}

// FindAll finds all users inside the database and returns the collection
// of users and the error in case somtheing happens.
func (u *User) FindAll() (entity.ModelInterface, error) {
	const op errors.Op = "service.User.FindAll"
	uu, err := u.repo.FindAll()

	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}

	return uu, nil
}

// FindBy finds one or more users based on an specific field.
func (u *User) FindBy(field string, value string) (entity.ModelInterface, error) {
	const op errors.Op = "service.User.FindBy"
	user, err := u.repo.FindBy(field, value)
	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}

	return user, nil
}

// Find Get a user based on its id and returns it, also can return an error
// if the search failed.
func (u *User) Find(id uint) (entity.ModelInterface, error) {
	const op errors.Op = "service.User.Find"
	model, err := u.repo.Find(id)

	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}

	return model, nil
}

// Validate validates the blog structure.
func (u *User) Validate(entity entity.ModelInterface) error {
	const op errors.Op = "service.User.Validate"
	err := validator.Validate(entity)
	if err != nil {
		err = errors.E(op, errors.KindUnprocesable, err)
		return err
	}
	return nil
}

// Insert creates a new user, and saves it to the database using the repository.
func (u *User) Insert(entity entity.ModelInterface) (entity.ModelInterface, error) {
	const op errors.Op = "service.User.Insert"

	err := u.Validate(entity)
	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}

	entity, err = u.repo.Insert(entity)
	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}

	return entity, nil
}

// Delete deletes a user based on it's Id.
func (u *User) Delete(id uint) error {
	const op errors.Op = "service.User.Delete"
	if err := u.repo.Delete(id); err != nil {
		err = errors.E(op, err)
		return err
	}
	return nil
}

// Update updates a user based on its id and the data sent to it.
func (u *User) Update(entity entity.ModelInterface, id uint) (entity.ModelInterface, error) {
	const op errors.Op = "service.User.Update"
	var err error
	entity, err = u.repo.Update(entity, id)
	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}
	return entity, nil
}

// Login the user and return a jwtToken or an error.
func (u *User) Login(credentials entity.LoginRequest) (string, error) {
	const op errors.Op = "service.User.Login"
	var (
		errWrongPassword = fmt.Errorf("Wrong password")
		token            string
	)

	if err := validator.Validate(credentials); err != nil {
		err = errors.E(op, errors.KindUnprocesable, err)
		return "", err
	}

	result, err := u.FindBy("email", credentials.Email)
	if err != nil {
		err = errors.E(op, err)
		return "", err
	}

	user := result.(entity.User)
	password := []byte(credentials.Password)
	hashPass := []byte(user.Password)

	err = bcrypt.CompareHashAndPassword(hashPass, password)
	if err != nil {
		err = errors.E(op, errors.KindUnprocesable, errWrongPassword)
		return "", err
	}

	token, err = jwtToken.GenerateJWT(credentials.Email, string(password))
	if err != nil {
		err = errors.E(op, errors.KindUnprocesable, errWrongPassword)
		return "", err
	}

	return token, nil
}
