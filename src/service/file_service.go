// serivce defines all structs and functions for the service layer.
// Author: wmb1207
package service

import (
	"blog/entity"
	"blog/errors"
	"blog/repository"
	"bytes"
	"fmt"
	"io"
	"mime/multipart"
	"os"
	"path/filepath"
	"time"
)

// Struct File used to represent the file service.
// Implements service Interface
//	FindAll() (entity.ModelInterface, error)
//	Find(id uint) (entity.ModelInterface, error)
//	Insert(model entity.ModelInterface) (entity.ModelInterface, error)
//	Delete(id uint) error
//	Update(model entity.ModelInterface, id uint) (entity.ModelInterface, error)
//	FindBy(field string, value string) (entity.ModelInterface, error)
//  Validate(entity entity.ModelInterface) error
type File service

// NewFile Return a pointer to a new instance of service.File.
func NewFile(repo *repository.File) *File {
	return &File{
		repo: repo,
	}
}

// FindAll finds all users inside the database and returns the collection
// of users and the error in case somtheing happens.
func (f *File) FindAll() (entity.ModelInterface, error) {
	const op errors.Op = "service.File.FindAll"
	err := errors.E(op, errors.KindUnavailable, fmt.Errorf("Method Undefined in comment service."))
	return nil, err
}

// FindBy is Not implemented.
func (f *File) FindBy(field string, value string) (entity.ModelInterface, error) {
	const op errors.Op = "service.File.FindBy"
	err := errors.E(op, errors.KindUnavailable, fmt.Errorf("Method Undefined in comment service."))
	return nil, err
}

// Validate validates the File structure.
func (f *File) Validate(entity entity.ModelInterface) error {
	const op errors.Op = "service.File.Validate"
	err := errors.E(op, errors.KindUnavailable, fmt.Errorf("Method Undefined in comment service."))
	return err
}

// Find Get a File entity based on the id.
func (f *File) Find(id uint) (entity.ModelInterface, error) {
	const op errors.Op = "sercie.File.Find"
	model, err := f.repo.Find(id)
	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}

	return model, nil
}

// Insert creates a new file, and saves it to the database using the repository.
func (f *File) Insert(entity entity.ModelInterface) (entity.ModelInterface, error) {
	const op errors.Op = "service.File.Insert"
	var err error

	entity, err = f.repo.Insert(entity)
	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}

	return entity, nil
}

// Delete deletes a file based on it's Id.
func (f *File) Delete(id uint) error {
	const op errors.Op = "service.File.Delete"
	if err := f.repo.Delete(id); err != nil {
		err = errors.E(op, err)
		return err
	}
	return nil
}

// Update updates a file based on its id and the data sent to it.
func (f *File) Update(entity entity.ModelInterface, id uint) (entity.ModelInterface, error) {
	const op errors.Op = "service.File.Update"
	var err error
	entity, err = f.repo.Update(entity, id)
	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}
	return entity, nil
}

// SaveFile saves a file to a directory/file.
func (f *File) SaveFile(file *multipart.FileHeader, dst string) error {
	src, err := file.Open()
	if err != nil {
		return err
	}
	defer src.Close()

	out, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer out.Close()

	_, err = io.Copy(out, src)
	return err
}

// UploadFiles takes a multipart form, and uploads all files.
func (f *File) UploadFiles(form *multipart.Form) (entity.Files, error) {
	const op errors.Op = "service.File.UploadFiles"
	var (
		paths         entity.Files
		ErrFileExists error = fmt.Errorf("File already Exists")
	)

	files := form.File["upload[]"]

	for _, file := range files {
		filename := filepath.Base(file.Filename)
		dir := f.getDir()

		if !f.dirExist(dir) {
			os.Mkdir(dir, os.ModePerm)
		}

		path := f.generatePath(dir, filename)

		if f.fileExist(path) {
			err := errors.E(op, ErrFileExists)
			return paths, err
		}

		if err := f.SaveFile(file, path); err != nil {
			err = errors.E(op, err)
			return paths, err
		}

		currentFile := entity.FileRequest{
			Name:      filename,
			Directory: path,
		}

		fileSaved, err := f.toDB(currentFile)
		if err != nil {
			err = errors.E(op, err)
			return paths, err
		}

		paths = append(paths, fileSaved)
	}

	return paths, nil

}

// Save Files into the database.
func (f *File) toDB(file entity.FileRequest) (entity.File, error) {
	const op errors.Op = "fileController.toDB"
	var (
		err    error
		result entity.ModelInterface
	)
	newFile := entity.File{
		FileTitle: file.Name,
		FileURL:   file.Directory,
		BlogID:    0,
	}

	result, err = f.Insert(newFile)
	if err != nil {
		err = errors.E(op, err)
	}
	newFile = result.(entity.File)
	return newFile, err
}

// getDir generates the directory where the file should be stored.
func (f *File) getDir() string {
	var buffer bytes.Buffer
	date := time.Now()
	buffer.WriteString("static/")
	buffer.WriteString(date.Format("01-02-2006/"))
	return buffer.String()
}

// generatePath Takes a directory and a file name and concatenates them.
func (f *File) generatePath(dir string, filename string) string {
	var buffer bytes.Buffer
	buffer.WriteString(dir)
	buffer.WriteString(filename)
	return buffer.String()

}

// exist checks if the path beign that a file url, or a directory exists.
func (f *File) exist(path string) bool {
	if _, err := os.Stat(path); err != nil {
		return false
	}
	return true
}

// dirExist wrapper for exist.
func (f *File) dirExist(dir string) bool {
	return f.exist(dir)
}

// dirExist wrapper for exist.
func (f *File) fileExist(path string) bool {
	return f.exist(path)
}
