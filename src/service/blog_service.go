// Package Serivce defines all structs and functions for the service layer.
// Author: wmb1207
package service

import (
	"blog/entity"
	"blog/errors"
	"blog/repository"
	"blog/validator"
	"fmt"
)

// Struct Blog used to represent the blog service.
// Implements service Interface
//	FindAll() (entity.ModelInterface, error)
//	Find(id uint) (entity.ModelInterface, error)
//	Insert(model entity.ModelInterface) (entity.ModelInterface, error)
//	Delete(id uint) error
//	Update(model entity.ModelInterface, id uint) (entity.ModelInterface, error)
//	FindBy(field string, value string) (entity.ModelInterface, error)
//  Validate(entity entity.ModelInterface) error
type Blog struct {
	service
	userRepo repository.RepositoryInterface
}

// NewBlog Return a pointer to a new instance of service.Blog.
func NewBlog(repo *repository.Blog, userRepo *repository.User) *Blog {
	return &Blog{
		service{
			repo: repo,
		},
		userRepo,
	}
}

// FindAll finds all blogs inside the database and returns the collection
// of blogs and the error in case somtheing happens.
func (b *Blog) FindAll() (entity.ModelInterface, error) {
	const op errors.Op = "service.Blog.FindAll"
	bb, err := b.repo.FindAll()
	// Here i should define the error message instead of inside the repo.
	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}

	return bb, nil
}

// FindBy is not implemented here so it return nil and the error undefined method.
func (b *Blog) FindBy(field string, value string) (entity.ModelInterface, error) {
	const op errors.Op = "service.Blog.FindBy"
	err := errors.E(op, errors.KindUnavailable, fmt.Errorf("Method Undefined in blog service."))
	return nil, err
}

// Find Get a blog based on its id and returns it, also can return an error
// if the search failed.
func (b *Blog) Find(id uint) (entity.ModelInterface, error) {
	const op errors.Op = "service.Blog.FindAll"
	model, err := b.repo.Find(id)

	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}

	return model, nil
}

// Validate validates the blog structure.
func (b *Blog) Validate(entity entity.ModelInterface) error {
	const op errors.Op = "service.Blog.Validate"
	err := validator.Validate(entity)
	if err != nil {
		err = errors.E(op, errors.KindUnprocesable, err)
		return err
	}
	return nil
}

// Insert creates a new blog, and saves it to the database using the repository.
// Also finds the user based on the user id found in the blog strcuture.
func (b *Blog) Insert(request entity.ModelInterface) (entity.ModelInterface, error) {
	const op errors.Op = "service.Blog.Insert"
	var userResult entity.ModelInterface

	err := b.Validate(request)
	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}

	blog, ok := request.(entity.Blog)
	if !ok {
		err = errors.E(op, errors.KindUnprocesable, fmt.Errorf("Wrong structure."))
		return nil, err
	}

	userResult, err = b.userRepo.Find(blog.UserID)
	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}

	user := userResult.(entity.User)
	blog.User = user

	request, err = b.repo.Insert(blog)
	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}

	return request, nil
}

// Delete deletes a blog based on it's Id.
func (b *Blog) Delete(id uint) error {
	const op errors.Op = "service.Blog.Delete"
	if err := b.repo.Delete(id); err != nil {
		err = errors.E(op, err)
		return err
	}
	return nil
}

// Update updates a blog based on its id and the data sent to it.
func (b *Blog) Update(entity entity.ModelInterface, id uint) (entity.ModelInterface, error) {
	const op errors.Op = "service.Blog.Update"
	var err error
	entity, err = b.repo.Update(entity, id)
	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}
	return entity, nil
}
