// serivce defines all structs and functions for the service layer.
// Author: wmb1207
package service

import (
	"blog/entity"
	"blog/errors"
	"blog/repository"
	"fmt"
	"time"

	"github.com/dgrijalva/jwt-go"
)

// Struct RestorePass used to represent the restore password service.
// Implements service Interface
//	FindAll() (entity.ModelInterface, error)
//	Find(id uint) (entity.ModelInterface, error)
//	Insert(model entity.ModelInterface) (entity.ModelInterface, error)
//	Delete(id uint) error
//	Update(model entity.ModelInterface, id uint) (entity.ModelInterface, error)
//	FindBy(field string, value string) (entity.ModelInterface, error)
//  Validate(entity entity.ModelInterface) error
type RestorePass struct {
	service
	userRepo repository.RepositoryInterface
}

// NewRestorePass Return a pointer to a new instance of service.RestorePass.
func NewRestorePass(repo *repository.RestorePass, userRepo *repository.User) *RestorePass {
	return &RestorePass{
		service{
			repo: repo,
		},
		userRepo,
	}
}

// FindAll finds all toens inside the database and returns the collection
// of tokens and the error in case somtheing happens.
func (r *RestorePass) FindAll() (entity.ModelInterface, error) {
	const op errors.Op = "service.RestorePass.FindAll"
	tt, err := r.repo.FindAll()

	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}

	return tt, err
}

// Find Get a token based on its id and returns it, also can return an error
// if the search failed.
func (r *RestorePass) Find(id uint) (entity.ModelInterface, error) {
	const op errors.Op = "service.RestorePass.Find"
	t, err := r.repo.Find(id)
	if err != nil {
		err = errors.E(err, op)
		return nil, err
	}

	return t, err
}

// FindBy is Not implemented.
func (r *RestorePass) FindBy(field string, value string) (entity.ModelInterface, error) {
	const op errors.Op = "service.RestorePass.FindBy"
	err := errors.E(op, errors.KindUnavailable, fmt.Errorf("Method Undefined in restorePass service."))
	return nil, err
}

// Validate is Not implemented.
func (r *RestorePass) Validate(entity entity.ModelInterface) error {
	const op errors.Op = "service.RestorePass.Validate"
	err := errors.E(op, errors.KindUnavailable, fmt.Errorf("Method Undefined in restorePass service."))
	return err
}

// FindExpiredOnes Get an slice of all expired tokens.
func (r *RestorePass) FindExpiredOnes() (entity.ModelInterface, error) {
	const op errors.Op = "service.RestorePass.FindExpiredOnes"
	var (
		result            entity.ModelInterface
		restorePassTokens entity.RestorePassTokens
		expireOnes        entity.RestorePassTokens
		err               error
		ok                bool
	)

	expireOnes = entity.RestorePassTokens{}

	if result, err = r.FindAll(); err != nil {
		err = errors.E(op, err)
		return nil, err
	}

	if restorePassTokens, ok = result.(entity.RestorePassTokens); !ok {
		err = errors.E(op, errors.KindUnprocesable, err)
		return nil, err
	}

	for _, tokenItem := range restorePassTokens.RestorePassTokens {
		age := time.Since(tokenItem.CreatedAt)
		if age.Hours() >= 24 {
			expireOnes.RestorePassTokens = append(expireOnes.RestorePassTokens, tokenItem)
		}
	}

	expireOnes.Length = len(expireOnes.RestorePassTokens)
	return expireOnes, nil
}

// BulkDelete deletes a group of restore tokens.
func (r *RestorePass) BulkDelete(request entity.ModelInterface) error {
	const op errors.Op = "service.RestorePass.BulkDelete"
	var err error
	tokens, ok := request.(*entity.RestorePassTokens)
	if !ok {
		err = errors.E(op, errors.KindUnprocesable, fmt.Errorf("Error not correct entity RestorePassTokens"))
		return err
	}

	for _, token := range tokens.RestorePassTokens {
		if err = r.Delete(token.ID); err != nil {
			err = errors.E(op, err)
			return err
		}
	}

	return nil
}

// Delete deletes a restore token based on the id value.
func (r *RestorePass) Delete(id uint) error {
	const op errors.Op = "service.RestorePass.Delete"
	_, err := r.Find(id)
	if err != nil {
		err = errors.E(op, err)
		return err
	}

	r.repo.Delete(id)
	return nil
}

// Insert Creates a new restore token in the Database.
func (r *RestorePass) Insert(request entity.ModelInterface) (entity.ModelInterface, error) {
	const op errors.Op = "service.RestorePass.Insert"
	var (
		restorePassToken entity.RestorePassToken
		ok               bool
	)

	if restorePassToken, ok = request.(entity.RestorePassToken); !ok {
		err := errors.E(op, errors.KindUnprocesable, fmt.Errorf("Invalid Request Body"))
		return nil, err
	}

	response, err := r.repo.Insert(restorePassToken)
	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}
	return response, nil
}

// Update not implemented only defined to implement the repository interface
func (r *RestorePass) Update(request entity.ModelInterface, id uint) (entity.ModelInterface, error) {
	const op errors.Op = "service.RestorePass.Update"
	errString := fmt.Errorf("Method undefined")
	err := errors.E(op, errors.KindUnavailable, errString)
	return nil, err
}

// GenerateTemporalToken generates a temporal token for restore the password.
func (r *RestorePass) GenerateTemporalToken(request entity.RestorePasswordRequest) (string, error) {
	const op errors.Op = "service.RestorePass.GenerateTemporalToken"
	var newTemporalToken string

	userFound, err := r.userRepo.FindBy("email", request.Email)
	if err != nil {
		err = errors.E(op, err)
		return "", err
	}

	user := userFound.(entity.User)
	newTemporalToken, err = r.newToken(user.Password, user.Email)
	if err != nil {
		err = errors.E(op, err)
		return "", err
	}

	restorePassStruct := entity.RestorePassToken{
		Email: request.Email,
		Token: newTemporalToken,
	}

	_, err = r.repo.Insert(restorePassStruct)
	if err != nil {
		err = errors.E(op, err)
		return "", err
	}

	return newTemporalToken, nil

}

// newToken creates a new token to be used as the temporal token.
func (r *RestorePass) newToken(password string, email string) (string, error) {
	const op errors.Op = "service.RestorePass.newToken"
	signingKey := []byte(password)
	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)
	claims["autohorized"] = true
	claims["user"] = email
	claims["exp"] = time.Now().Add(time.Hour * 24).Unix()

	tokenString, err := token.SignedString(signingKey)

	if err != nil {
		err = errors.E(op, err)
		return "", err
	}

	return tokenString, nil
}

// ResetPassword takes the token passed
func (r *RestorePass) ResetPassword(requestToken string, request entity.NewPasswordRequest) (entity.ModelInterface, error) {
	const op errors.Op = "service.RestorePass.ResetPassword"
	var (
		storedToken  entity.RestorePassToken
		response     entity.ModelInterface
		responseUser entity.User
		password     string
	)
	_, err := jwt.Parse(requestToken, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			err := errors.E(op, errors.KindUnprocesable, fmt.Errorf("Error wrong body request"))
			return nil, err
		}

		if !token.Valid {
			err := errors.E(op, errors.KindUnprocesable, fmt.Errorf("Error wrong body request"))
			return nil, err
		}

		tokenClaims := token.Claims.(jwt.MapClaims)
		userEmail, ok := tokenClaims["user"].(string)
		if !ok {
			err := errors.E(op, errors.KindUnprocesable, fmt.Errorf("Error wrong body request"))
			return nil, err
		}

		result, err := r.repo.FindBy("email", userEmail)
		if err != nil {
			err = errors.E(op, errors.KindUnprocesable, err)
			return nil, err
		}

		user := result.(entity.User)
		pass := []byte(request.Password)
		password, err = repository.HashAndSalt(pass)
		if err != nil {
			err = errors.E(op, errors.KindUnprocesable, err)
			return nil, err
		}

		user.Password = password
		storedToken, ok = result.(entity.RestorePassToken)
		if !ok {
			err = errors.E(op, errors.KindUnprocesable, err)
			return nil, err
		}

		if err = r.repo.Delete(storedToken.ID); err != nil {
			err = errors.E(op, errors.KindUnprocesable, err)
			return nil, err
		}

		response, err = r.userRepo.Update(user, user.ID)
		if err != nil {
			err = errors.E(op, errors.KindUnprocesable, err)
			return nil, err
		}

		user, ok = response.(entity.User)
		if !ok {
			err = errors.E(op, errors.KindUnprocesable, err)
			return nil, err
		}
		responseUser = user
		return nil, nil
	})
	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}

	return responseUser, nil
}
