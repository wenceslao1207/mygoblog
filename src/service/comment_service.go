// Package Serivce defines all structs and functions for the service layer.
// Author: wmb1207
package service

import (
	"blog/entity"
	"blog/errors"
	"blog/repository"
	"blog/validator"
	"fmt"
)

// Struct Comment used to represent the comment service.
// Implements service Interface
//	FindAll() (entity.ModelInterface, error)
//	Find(id uint) (entity.ModelInterface, error)
//	Insert(model entity.ModelInterface) (entity.ModelInterface, error)
//	Delete(id uint) error
//	Update(model entity.ModelInterface, id uint) (entity.ModelInterface, error)
//	FindBy(field string, value string) (entity.ModelInterface, error)
//  Validate(entity entity.ModelInterface) error
type Comment struct {
	service
	userRepo repository.RepositoryInterface
}

// NewComment Return a pointer to a new instance of service Comment.
func NewComment(repo *repository.Comment, userRepo *repository.User) *Comment {
	return &Comment{
		service{
			repo: repo,
		},
		userRepo,
	}
}

// FindAll finds all comments inside the database and returns the collection
// of comments and the error in case somtheing happens.
func (c *Comment) FindAll() (entity.ModelInterface, error) {
	const op errors.Op = "service.Comment.FindAll"
	cc, err := c.repo.FindAll()

	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}

	return cc, nil
}

// FindBy is not implemented here so it return nil and the error undefined method.
func (c *Comment) FindBy(field string, value string) (entity.ModelInterface, error) {
	const op errors.Op = "service.Comment.FindBy"
	err := errors.E(op, errors.KindUnavailable, fmt.Errorf("Method Undefined in comment service."))
	return nil, err
}

// Find Get a comment based on its id and returns it, also can return an error
// if the search failed.
func (c *Comment) Find(id uint) (entity.ModelInterface, error) {
	const op errors.Op = "service.Comment.FindAll"
	model, err := c.repo.Find(id)

	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}

	return model, nil
}

// Validate validates the blog structure.
func (c *Comment) Validate(entity entity.ModelInterface) error {
	const op errors.Op = "service.Blog.Validate"
	err := validator.Validate(entity)
	if err != nil {
		err = errors.E(op, errors.KindUnprocesable, err)
		return err
	}
	return nil
}

// Insert creates a new comment, and saves it to the database using the repository.
func (c *Comment) Insert(request entity.ModelInterface) (entity.ModelInterface, error) {
	const op errors.Op = "service.Blog.Insert"
	var userResult entity.ModelInterface

	err := c.Validate(request)
	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}

	comment, ok := request.(entity.Comment)
	if !ok {
		err = errors.E(op, errors.KindUnprocesable, fmt.Errorf("Wrong structure."))
		return nil, err
	}

	userResult, err = c.userRepo.Find(comment.UserID)
	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}

	user := userResult.(entity.User)
	comment.User = user

	request, err = c.repo.Insert(comment)
	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}

	return request, nil
}

// Delete deletes a comment based on it's Id.
func (c *Comment) Delete(id uint) error {
	const op errors.Op = "service.Blog.Delete"
	if err := c.repo.Delete(id); err != nil {
		err = errors.E(op, err)
		return err
	}
	return nil
}

// Update updates a blog based on its id and the data sent to it.
func (c *Comment) Update(entity entity.ModelInterface, id uint) (entity.ModelInterface, error) {
	const op errors.Op = "service.Blog.Update"
	var err error
	entity, err = c.repo.Update(entity, id)
	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}
	return entity, nil
}
