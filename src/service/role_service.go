// Package Serivce defines all structs and functions for the service layer.
// Author: wmb1207
package service

import (
	"blog/entity"
	"blog/errors"
	"blog/repository"
	"blog/validator"
)

// Struct Comment used to represent the role service.
// Implements service Interface
//	FindAll() (entity.ModelInterface, error)
//	Find(id uint) (entity.ModelInterface, error)
//	Insert(model entity.ModelInterface) (entity.ModelInterface, error)
//	Delete(id uint) error
//	Update(model entity.ModelInterface, id uint) (entity.ModelInterface, error)
//	FindBy(field string, value string) (entity.ModelInterface, error)
//  Validate(entity entity.ModelInterface) error
type Role service

func NewRole(repo *repository.Role) *Role {
	return &Role{
		repo: repo,
	}
}

// FindAll finds all roles inside the database and returns the collection
// of roles and the error in case somtheing happens.
func (r *Role) FindAll() (entity.ModelInterface, error) {
	const op errors.Op = "service.Role.FindAll"
	rr, err := r.repo.FindAll()

	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}

	return rr, nil
}

// FindBy finds one or more roles based on an specific field.
func (r *Role) FindBy(field string, value string) (entity.ModelInterface, error) {
	const op errors.Op = "service.Role.FindBy"
	role, err := r.repo.FindBy(field, value)
	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}

	return role, nil
}

// Find Get a user based on its id and returns it, also can return an error
// if the search failed.
func (r *Role) Find(id uint) (entity.ModelInterface, error) {
	const op errors.Op = "service.Role.FindAll"
	model, err := r.repo.Find(id)

	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}

	return model, nil
}

// Validate validates the blog structure.
func (r *Role) Validate(entity entity.ModelInterface) error {
	const op errors.Op = "service.Role.Validate"
	err := validator.Validate(entity)
	if err != nil {
		err = errors.E(op, errors.KindUnprocesable, err)
		return err
	}
	return nil
}

// Insert creates a new user, and saves it to the database using the repository.
func (r *Role) Insert(entity entity.ModelInterface) (entity.ModelInterface, error) {
	const op errors.Op = "service.Role.Insert"

	err := r.Validate(entity)
	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}

	entity, err = r.repo.Insert(entity)
	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}

	return entity, nil
}

// Delete deletes a user based on it's Id.
func (r *Role) Delete(id uint) error {
	const op errors.Op = "service.Role.Delete"
	if err := r.repo.Delete(id); err != nil {
		err = errors.E(op, err)
		return err
	}
	return nil
}

// Update updates a blog based on its id and the data sent to it.
func (r *Role) Update(entity entity.ModelInterface, id uint) (entity.ModelInterface, error) {
	const op errors.Op = "service.Role.Update"
	var err error
	entity, err = r.repo.Update(entity, id)
	if err != nil {
		err = errors.E(op, err)
		return nil, err
	}
	return entity, nil
}
