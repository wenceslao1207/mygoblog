// Package validator contains structs, vars, and function used to validate fields in structs.
// Author: wmb1207
package validator

import (
	"fmt"
	"reflect"
	"regexp"
	"strings"
)

// Name of the tag used.
const tagName = "validator"

// Regular expression used to validate emails.
var mailRegex = regexp.MustCompile(`\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z`)

// Regular expression used to validate passwords.
// Passwords should have at least:
//	- 8 Characters.
//	- One uppercase character.
//	- One lowercase character.
//	- One Special character.
var passRegex = regexp.MustCompile(`[a-zA-Z\d+\D+]{8,}`)

type (
	// Genertic data validator.
	Validator interface {
		Validate(interface{}) (bool, error)
	}
	// Struct EmailValidator used to implement the function Validate
	// for the email tag.
	EmailValidator struct{}
	// Struct PasswordValidator used to implement the function Validate
	// for the password tag.
	PasswordValidator struct{}
	// Struct RequiredValidator used to implement the function Validate
	// for the require tag.
	RequiredValidator struct{}
	// Struct EmailValidator used to implement the default Validate
	// function.
	DefaultValidator struct{}
	// Struct used to represent the array of errors posible for multiple
	// validators.
	validError struct {
		errors []error
	}
)

// Implementation of the error interface for the validError struct.
func (v validError) Error() string {
	str := ""
	for _, err := range v.errors {
		str += err.Error() + " "
	}
	return str
}

// Default validator doesn't validate truly just returns true.
func (v DefaultValidator) Validate(val interface{}) (bool, error) {
	return true, nil
}

// Passowrd Validator (A password should have at least a minimum of eight characters
// One uppercase letter, one lower case letter, one number, and one special character).
func (v PasswordValidator) Validate(val interface{}) (bool, error) {
	if !passRegex.MatchString(val.(string)) {
		return false, fmt.Errorf("Password should have a minimum eight characters" +
			", at least one uppercase letter, one lowercase letter, one number and one" +
			" special character.")
	}
	return true, nil
}

// Required validator checks if a value us setted.
func (v RequiredValidator) Validate(val interface{}) (bool, error) {
	if val == nil {
		return false, fmt.Errorf("Empty required field found.")
	}
	return true, nil
}

// Email validator checks the email based on the mailRegex.
func (v EmailValidator) Validate(val interface{}) (bool, error) {
	if !mailRegex.MatchString(val.(string)) {
		return false, fmt.Errorf("Not a valid email.")
	}
	return true, nil
}

// Function getValidatorFromTags checks the tags for each field of the struct
// and returns the correct validator for each one.
func getValidatorFromTags(tag string) []Validator {
	// Generate the slice of validators
	args := strings.Split(tag, ",")

	// Create the slice with the default validator.
	validators := []Validator{DefaultValidator{}}
	for _, arg := range args {
		switch arg {
		case "email":
			validators = append(validators, EmailValidator{})
		case "required":
			validators = append(validators, RequiredValidator{})
		case "password":
			validators = append(validators, PasswordValidator{})
		}
	}

	return validators
}

// Perform an actual validation.
func Validate(s interface{}) error {
	errs := validError{}

	// ValueOf return a value representing the run-time data.
	v := reflect.ValueOf(s)
	for i := 0; i < v.NumField(); i++ {
		tag := v.Type().Field(i).Tag.Get(tagName)
		if tag == "" || tag == "-" {
			continue
		}

		//  Get a validator that correspondes to a tag
		validators := getValidatorFromTags(tag)

		// Perform validation
		for _, validator := range validators {
			valid, err := validator.Validate(v.Field(i).Interface())

			// Append error to slice.
			if !valid && err != nil {
				errs.errors = append(
					errs.errors,
					fmt.Errorf("%s, %s", v.Type().Field(i).Name, err.Error()),
				)
			}
		}
	}
	// If any error exist return the array
	if len(errs.errors) > 0 {
		return errs
	}

	return nil
}
