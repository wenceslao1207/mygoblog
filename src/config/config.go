// Package config provides structures and functions for manage the json configuration file.
// Author:  wmb1207.
package config

import (
	"encoding/json"
	"log"
	"os"
)

// Struct configuration, represents the configuration for the site and how it should
// be parsed from the .config.json file
type configuration struct {
	Port     string `json:"port"`
	Env      string `json:"env"`
	LogDir   string `json:"loggin_dir,ommitempty"`
	BaseUrl  string `json:"base_url"`
	Database databaseConfig
	Email    emailConfig
}

// Strcut databaseConfig represents the configuration for the database.
type databaseConfig struct {
	Driver   string `json:"driver"`
	Database string `json:"database"`
	User     string `json:"user"`
	Password string `json"password"`
	Host     string `json"host"`
}

// Strcut emailConfig represents the configuration for the emil system.
type emailConfig struct {
	Service  string `json:"service"`
	Port     int    `json:"port"`
	User     string `json:"user"`
	Password string `json:"password"`
	MailFrom string `json:"mail_from"`
	Address  string `json:"mail_from_address"`
}

var config configuration

const configFile = ".config.json"

func init() {
	file, err := os.Open(".config.json")
	if err != nil {
		log.Println(err.Error())
	}
	defer file.Close()
	decoder := json.NewDecoder(file)
	config = configuration{}
	err = decoder.Decode(&config)
	if err != nil {
		log.Println(err.Error())
	}
}

// GetConfig returns a pointer to the configuration entity.
func GetConfig() *configuration {
	return &config
}
