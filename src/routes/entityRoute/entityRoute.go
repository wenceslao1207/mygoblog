// Package entityRoute define method and structs used to generate all generic endpoints for an entity.
// Autho: wmb1207.
package entityRoute

import (
	"blog/httpd/controller/controller"

	"github.com/gin-gonic/gin"
)

type (
	entityRouter struct {
		controller controller.ControllerInterface
		router     gin.IRouter
		route      string
	}

	middleware func()
)

func New(router gin.IRouter, route string, controller controller.ControllerInterface) *entityRouter {
	return &entityRouter{
		controller: controller,
		route:      route,
		router:     router,
	}
}

func (e *entityRouter) Router() {
	group := e.router.Group(e.route)
	{
		group.GET("/", e.controller.GetAll)
		group.POST("/", e.controller.Create)
		group.DELETE("/:id", e.controller.Delete)
		group.GET("/:id", e.controller.Get)
		group.PUT("/:id", e.controller.Update)
	}
}

func (e *entityRouter) RouterWithMiddleware(config map[string]gin.HandlerFunc) {
	group := e.router.Group(e.route)
	var methods = []string{"GetAll", "Get", "Delete", "Post", "Put"}

	for _, method := range methods {
		switch method {
		case "GetAll":
			if val, ok := config[method]; ok {
				group.GET("/", val, e.controller.GetAll)
			} else {
				group.GET("/", e.controller.GetAll)
			}
		case "Get":
			if val, ok := config[method]; ok {
				group.GET("/:id", val, e.controller.Get)
			} else {
				group.GET("/:id", e.controller.GetAll)
			}
		case "Delete":
			if val, ok := config[method]; ok {
				group.DELETE("/:id", val, e.controller.Delete)
			} else {
				group.DELETE("/:id", e.controller.Delete)
			}
		case "Post":
			if val, ok := config[method]; ok {
				group.POST("/", val, e.controller.Create)
			} else {
				group.POST("/", e.controller.Create)
			}
		case "Put":
			if val, ok := config[method]; ok {
				group.PUT("/:id", val, e.controller.Update)
			} else {
				group.PUT("/:id", e.controller.Update)
			}
		}
	}
}
