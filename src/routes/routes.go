// Package routes provides structures and functions for manage all routes in the site.
// Author:  wmb1207.
package routes

import (
	"blog/config"
	"blog/controller"
	"blog/middleware"
	"blog/service"

	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func StartGin() {
	cfg := config.GetConfig()
	router := gin.Default()

	router.Use(cors.New(cors.Config{
		AllowOrigins:  []string{"*"},
		AllowMethods:  []string{"GET, POST, PUT, DELETE"},
		AllowHeaders:  []string{"*"},
		ExposeHeaders: []string{"Content-Length"},
		MaxAge:        12 * time.Hour,
	}))

	middleware := middleware.New()
	// Controllers
	blog := controller.NewBlog(service.BlogService)
	user := controller.NewUser(service.UserService)
	login := controller.NewLogin(service.UserService)
	role := controller.NewRole(service.RoleService)
	comment := controller.NewComment(service.CommentService)
	files := controller.NewFile(service.FileService)
	restore := controller.NewRestorePass(service.RestorePassService)

	router.Use(middleware.ServerError())

	api := router.Group("/api")
	{
		api.POST("/login", login.Login)
	}
	api.Use(middleware.Cached())
	userRoutes := api.Group("/users")
	{
		userRoutes.GET("/", user.GetAll)
		userRoutes.POST("/", user.Create)
		userRoutes.DELETE("/:id", middleware.AuthRequired(), user.Delete)
		userRoutes.GET("/:id", user.Get)
		userRoutes.PUT("/:id", middleware.AuthRequired(), user.Update)
	}
	blogRoutes := api.Group("/blogs")
	{
		blogRoutes.GET("/", blog.GetAll)
		blogRoutes.POST("/", blog.Create)
		blogRoutes.GET("/:id", blog.Get)
		blogRoutes.DELETE("/:id", middleware.AuthRequired(), blog.Delete)
		blogRoutes.PUT("/:id", middleware.AuthRequired(), blog.Update)
	}
	commentRoutes := api.Group("/comments")
	{
		commentRoutes.GET("/", comment.GetAll)
		commentRoutes.POST("/", comment.Create)
		commentRoutes.DELETE("/:id", middleware.AuthRequired(), comment.Delete)
	}
	fileRoutes := api.Group("/files")
	{
		fileRoutes.POST("/", files.UploadFiles)
		fileRoutes.PUT("/:id", files.UploadFiles)
		fileRoutes.DELETE("/:id", files.DeleteFile)
	}
	restoreRoutes := api.Group("/restore/password")
	{
		restoreRoutes.POST("/", restore.RequestPasswordReset)
		restoreRoutes.POST("/:token", restore.ResetPassword)
	}
	rolesRoutes := api.Group("/roles")
	{
		rolesRoutes.GET("/", middleware.AuthRequired(), role.GetAll)
		rolesRoutes.POST("/", middleware.AuthRequired(), role.Create)
		rolesRoutes.GET("/:id", middleware.AuthRequired(), role.Get)
		rolesRoutes.DELETE("/:id", middleware.AuthRequired(), role.Delete)
	}
	router.Run(":" + cfg.Port)
}
