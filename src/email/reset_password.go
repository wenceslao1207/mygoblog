// Package email provides structures and functions for manage email related logic.
// Author: wmb1207.
package email

import (
	"blog/config"
)

// Struct RestorePassword  struct used to represent the restore password email.
// Composed by:
// - Pointer to email.Email
type RestorePassword struct {
	*Email
}

// Struct creationBody  composed by:
// - Url string
type restoreBody struct {
	Url string
}

// NewRestorePassword return the pointer to a new instance of RestorePassword.
func NewRestorePassword(token string, mail string) *RestorePassword {
	template := "./resources/templates/restore_password_email.html"
	config := config.GetConfig()
	url := config.BaseUrl + "/password/restore?token=" + token
	eb := restoreBody{
		Url: url,
	}
	e := New(eb)
	e.SetTemplate(template)
	e.SetDestinatary(mail)
	e.SetSubject("Reset password")
	rp := RestorePassword{
		Email: e,
	}
	return &rp
}
