// Package email provides a base structure and functions for manage
// all email needs.
// Author: wmb1207.
package email

import (
	"blog/config"
	"bytes"
	"html/template"

	"gopkg.in/gomail.v2"
)

// Struct EmailManager composed by:
// - Dialer gomail.Diales (used to send the email using gomail package)
// - From string (The name of the sender)
// - Address string (The email from it will be send)
type emailManager struct {
	Dialer  gomail.Dialer
	From    string
	Address string
}

var eManager emailManager

// Initialize the eManaeger using the configurations found at the .config.json
// file.
func init() {
	cfg := config.GetConfig()
	email := *gomail.NewDialer(
		cfg.Email.Service,
		cfg.Email.Port,
		cfg.Email.User,
		cfg.Email.Password)
	eManager = emailManager{
		Dialer:  email,
		From:    cfg.Email.MailFrom,
		Address: cfg.Email.Address,
	}
}

// GetEmailManager return a pointer to the current email manager (eManager).
func GetEmailManager() *emailManager {
	return &eManager
}

// Struct Email, basic structure for an email.
// Uses an html template.
type Email struct {
	Manager    *emailManager
	Message    *gomail.Message
	Template   template.Template
	BodyStruct interface{}
}

// New returns the pointer to a new instance of Email.
func New(body interface{}) *Email {
	e := Email{
		Manager:    GetEmailManager(),
		Message:    gomail.NewMessage(),
		BodyStruct: body,
	}
	e.Message.SetHeader("From", e.Manager.Address, e.Manager.From)
	return &e
}

// SetTemplate sets the template to the email, this template will be parsed to
// set all values from the structure used to define its content.
// Example struct used to set all vaues to a template:
//	type creationBody struct {
//		Name  string
//		Email string
//	}
func (e *Email) SetTemplate(temp string) error {
	t, err := template.ParseFiles(temp)
	if err != nil {
		return err
	}
	e.Template = *t
	return nil
}

// SetDestinatary, sets the destinatary email.
func (e *Email) SetDestinatary(email string) {
	e.Message.SetHeader("To", email)
}

// SetSubject, sets the subject to the email.
func (e *Email) SetSubject(subject string) {
	e.Message.SetHeader("Subject", subject)
}

// SetDestinataries, sets a group of destinataries.
func (e *Email) SetDestinataries(to map[string][]string) {
	e.Message.SetHeaders(to)
}

// SetCarbonCopy, sets a destinatary for a carbon copy.
func (e *Email) SetCarbonCopy(cc string) {
	e.Message.SetAddressHeader("Cc", cc, "")
}

// SetBlindCarbonCopy, sets a destinatary for a blind carbon copy.
func (e *Email) SetBlindCarbonCopy(bcc string) {
	e.Message.SetAddressHeader("Bcc", bcc, "")
}

// SetBody, sets the body of the email to be send.
func (e *Email) SetBody(contentType string, body string) {
	e.Message.SetBody(contentType, body)
}

func (e *Email) Send() error {
	if err := e.Manager.Dialer.DialAndSend(e.Message); err != nil {
		return err
	}
	return nil
}

// GenerateBody creates the body based on the template used converting it to
// an string before setting it as the body.
func (e *Email) GenerateBody() error {
	buff := new(bytes.Buffer)
	if err := e.Template.Execute(buff, e.BodyStruct); err != nil {
		return err
	}
	e.Message.SetBody("text/html", buff.String())
	return nil
}
