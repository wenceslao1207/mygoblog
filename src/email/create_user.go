// Package email provides a base structure and functions for manage
// all email needs.
// Author: wmb1207.
package email

// Struct CreateAccountConfirmation struct used to represent the create account confirmation
// email.
// Composed by:
// - Pointer to email.Email
type CreateAccountConfirmation struct {
	*Email
}

// Struct creationBody  composed by:
// - Name string
// - Email string
type creationBody struct {
	Name  string
	Email string
}

// NewCreateAccountConfirmation return the pointer to a new instance of CreateAccountConfirmation.
func NewCreateAccountConfirmation(name string, mail string) *CreateAccountConfirmation {
	template := "./resources/templates/create_account_confirmation.html"
	eb := creationBody{
		Name:  name,
		Email: mail,
	}
	e := New(eb)
	e.SetTemplate(template)
	e.SetDestinatary(mail)
	e.SetSubject("Accoutn creation confirmation")
	cac := CreateAccountConfirmation{
		Email: e,
	}
	return &cac
}
