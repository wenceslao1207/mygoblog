// Package cache provides structures and functions for manage the cache in memory.
// Author: wmb1207.
package cache

import (
	"time"

	"github.com/patrickmn/go-cache"
)

// The cache helper that should be initialized only once.
var cacheHelper CacheHelper

// Struct Item defines de strcuture of an item in the cache, using it's key field
// as the key in the cache.
type Item struct {
	Key            string
	Value          interface{}
	ExpirationTime time.Duration
}

// Struct CacheHelper strcut used to initilialize the cache system, and operate
// overt the cache.
type CacheHelper struct {
	Cache          cache.Cache
	ExpirationTime time.Duration
	PurgeTime      time.Duration
}

// Init.
func init() {
	cacheHelper = *New(5, 10)
}

// GetCacheHelper get the pointer to the cacheHelper variable.
func GetCacheHelper() *CacheHelper {
	return &cacheHelper
}

// CacheInit() Initialize de cache system.
func (c *CacheHelper) CacheInit() {
	c.Cache = *cache.New(c.ExpirationTime, c.PurgeTime)
}

// NewCache generates a pointer to a new instance of CacheHelper.
func New(expiration time.Duration, purge time.Duration) *CacheHelper {
	expirationTime := expiration * time.Minute
	purgeTime := purge * time.Minute
	return &CacheHelper{
		Cache:          cache.Cache{},
		ExpirationTime: expirationTime,
		PurgeTime:      purgeTime,
	}
}

// Set, inserts a new item in the cache.
func (c *CacheHelper) Set(item Item) {
	if item.ExpirationTime == 0*time.Minute {
		item.ExpirationTime = c.ExpirationTime
	}
	c.Cache.Set(item.Key, item.Value, item.ExpirationTime)
}

// Get, gets an item from the cache.
func (c *CacheHelper) Get(key string) (interface{}, bool) {
	result, found := c.Cache.Get(key)
	return result, found
}

// Delete, deletes an item from the cache.
func (c *CacheHelper) Delete(key string) {
	c.Cache.Delete(key)
}

// Update, updates an item on the cache, to do this first deltes
// the current item, and creates a new one.
func (c *CacheHelper) Update(item Item) {
	c.Delete(item.Key)
	c.Set(item)
}
