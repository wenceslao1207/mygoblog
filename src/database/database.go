// Package database provides structures and functions for manage the connection to the database.
// Author: wmb1207.
package database

// Interface Databaser used to define any database.
type Databaser interface {
	Connect() error
}

// Struct Database, used to represents the database connection with all the information need it.
type Credentials struct {
	Driver   string
	Database string
	User     string
	Password string
	Host     string
}
