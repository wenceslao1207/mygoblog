// Package database provides structures and functions for manage the connection to the database.
// Author: wmb1207.
package database

import (
	"blog/config"
	"blog/entity"
	"bytes"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

// Struct Mysql is an implementation for the database.
type Mysql struct {
	Credentials
	DB *gorm.DB
}

// db the database connection.
var db Mysql

// getConnectionString generates the connection string based on the credentials.
func (d *Mysql) getConnectionString() string {
	var buffer bytes.Buffer
	var result string

	// Set the connection string
	buffer.WriteString(d.User)
	buffer.WriteString(":")
	buffer.WriteString(d.Password)
	buffer.WriteString("@(")
	buffer.WriteString(d.Host)
	buffer.WriteString(")/")
	buffer.WriteString(d.Database)
	buffer.WriteString("?charset=utf8&parseTime=True&loc=Local")

	// Convert buffer to string
	result = buffer.String()
	return result
}

// Connect performs the connection to the database.
func (d *Mysql) Connect() error {
	db, err := gorm.Open(d.Driver, d.getConnectionString())
	if err != nil {
		return err
	}
	d.DB = db
	return nil
}

// Perform the connection an migrates all models to the datbase.
func init() {
	cfg := config.GetConfig()
	db = Mysql{
		Credentials: Credentials{
			Driver:   cfg.Database.Driver,
			Database: cfg.Database.Database,
			User:     cfg.Database.User,
			Password: cfg.Database.Password,
			Host:     cfg.Database.Host,
		},
	}
	db.Connect()
	db.DB.AutoMigrate(&entity.Blog{})
	db.DB.AutoMigrate(&entity.User{})
	db.DB.AutoMigrate(&entity.File{})
	db.DB.AutoMigrate(&entity.Comment{})
	db.DB.AutoMigrate(&entity.Role{})
}

// GetDatabase return a pointer to the database to be used in the repositories.
func MysqlDB() Databaser {
	return &db
}
