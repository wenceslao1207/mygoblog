up:
	docker-compose up
dev:
	docker-compose restart app-blog
local:
	cd src && go run httpd/main.go
build:
	cd src && docker build -t blog_app --no-cache .

